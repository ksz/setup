$RepoDir = "$env:PUBLIC\Repo"
$ToolDir = "C:\Tools"

$FileExists = Test-Path -Path $env:PUBLIC\virtual_machine
if ($FileExists -eq $true) {$MyTarget = 2; Remove-Item -Path $env:PUBLIC\virtual_machine -Force}
else {$MyTarget = 1}

Start-Process -FilePath powershell.exe -ArgumentList `
"Invoke-Expression -Command $ToolDir\Windows10Debloater\Windows10Debloater.ps1; `
New-Item -Path $ToolDir\Windows10Debloater\Logs -ItemType Directory; `
Move-Item -Path $ToolDir\Windows10Debloater\*transcript*.txt -Destination $ToolDir\Windows10Debloater\Logs; `
Remove-Item -Path $env:PUBLIC\Desktop\setup.bat -Force" -Verb RunAs

$Documents = "0?_*.*"
foreach ($Files in $Documents)
{Copy-Item -Path $RepoDir\scripts\$Files -Destination $env:USERPROFILE\Documents -Force}
$Pictures = "*_user.*"
foreach ($Files in $Pictures)
{Copy-Item -Path $RepoDir\artwork\$Files -Destination $env:USERPROFILE\Pictures -Force}

$LanguageList = Get-WinUserLanguageList
$LanguageList.Add("pl-PL")
Set-WinUserLanguageList $LanguageList -Force
Set-WinUserLanguageList -LanguageList pl, en-US -Force

curl.exe -L https://aka.ms/wsl-debian-gnulinux -o $env:TEMP\DebianGNULinux.AppxBundle
Rename-Item -Path $env:TEMP\DebianGNULinux.AppxBundle -NewName $env:TEMP\Debian.zip
Expand-Archive -Path $env:TEMP\Debian.zip -DestinationPath $env:TEMP\Debian
Rename-Item -Path $(Get-ChildItem $env:TEMP\Debian\*x64.appx) -NewName $env:TEMP\Debian\Debian_x64.zip
Expand-Archive -Path $env:TEMP\Debian\Debian_x64.zip -DestinationPath $env:USERPROFILE\Debian
Start-Process -FilePath $env:USERPROFILE\Debian\debian.exe -Wait
$PublicWSL = wsl wslpath -u `"$env:PUBLIC`"
Start-Process -FilePath wsl.exe -ArgumentList "sh $PublicWSL/wsl_debian_setup.sh" -Wait

# curl.exe -L https://aka.ms/wsl-ubuntu-1804 -o $env:TEMP\Ubuntu_1804.appx
# Rename-Item -Path $env:TEMP\Ubuntu_1804.appx -NewName $env:TEMP\Ubuntu_1804.zip
# Expand-Archive -Path $env:TEMP\Ubuntu_1804.zip -DestinationPath $env:USERPROFILE\Ubuntu-18.04
# Start-Process -FilePath $env:USERPROFILE\Ubuntu-18.04\ubuntu1804.exe -Wait
# Start-Process -FilePath wsl.exe -ArgumentList '--distribution Ubuntu-18.04','sudo sh -c "apt update && apt -y upgrade"' -Wait
# Write-Host

# curl.exe -L https://aka.ms/wslubuntu2004 -o $env:TEMP\Ubuntu_2004.appx
# Rename-Item -Path $env:TEMP\Ubuntu_2004.appx -NewName $env:TEMP\Ubuntu_2004.zip
# Expand-Archive -Path $env:TEMP\Ubuntu_2004.zip -DestinationPath $env:USERPROFILE\Ubuntu-20.04
# Start-Process -FilePath $env:USERPROFILE\Ubuntu-20.04\ubuntu2004.exe -Wait
# Start-Process -FilePath wsl.exe -ArgumentList '--distribution Ubuntu-20.04','sudo sh -c "apt update && apt -y upgrade"' -Wait
# Write-Host

if ($MyTarget -ne 2) {
	Read-Host -Prompt "`nPress any key to hide Input Indicator...`n(Taskbar ---> Turn system icons on or off)"
	Start-Process -FilePath ms-settings:taskbar
	Wait-Process -Name SystemSettings

	Read-Host -Prompt "Press any key to change account picture...`n(Your info ---> Browse for one)"
	Start-Process -FilePath ms-settings:yourinfo
	Wait-Process -Name SystemSettings
}

$QuickAccess = "$env:USERPROFILE\Music\","$env:USERPROFILE\Videos\","C:\","D:\"
foreach ($Path in $QuickAccess)
{(New-Object -COMObject Shell.Application).Namespace("$Path").Self.InvokeVerb("pintohome")}

$Taskbar = "Microsoft Edge"
((New-Object -COMObject Shell.Application).NameSpace("shell:::{4234d49b-0245-4df3-b780-3893943456e1}").Items() |
?{$_.Name -eq $Taskbar}).Verbs() | ?{$_.Name.replace("&","") -match "Unpin from taskbar"} | %{$_.DoIt(); $exec = $true}
$FileExists = Test-Path -Path "$env:USERPROFILE\Desktop\Microsoft Edge.lnk"
if ($FileExists -eq $true) {Remove-Item -Path "$env:USERPROFILE\Desktop\Microsoft Edge.lnk" -Force}

taskkill /f /im explorer.exe
Start-Process -FilePath regedit.exe -ArgumentList "/s","$env:PUBLIC\windows10_HKCU.reg" -Wait
Remove-Item -Path $RepoDir -Recurse -Force

Get-Process -Name OneDrive -ErrorAction SilentlyContinue | Stop-Process -Force
Start-Process -FilePath $env:SystemRoot\SysWOW64\OneDriveSetup.exe -ArgumentList "/uninstall" -Wait

Read-Host -Prompt "`nPress any key to reboot machine..."
Restart-Computer
