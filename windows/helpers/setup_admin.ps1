Param (
	[switch]$PostReboot
)

$User1 = "admin"
$User2 = "ksz"
$ComputerName = "OP7050"
$RepoDir = "$env:PUBLIC\Repo"
$ToolDir = "C:\Tools"

function Start-RunAsAdmin {
	if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))
	{Start-Process -FilePath powershell.exe -ArgumentList "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit}

	$MyTarget = Read-Host "Choose installation target`n  1) physical machine 2) virtual machine"
	if ($MyTarget -eq 2) {New-Item -Path $env:PUBLIC\virtual_machine -ItemType File}

	Set-NetConnectionProfile -Name Network -InterfaceAlias Ethernet -NetworkCategory Private
	Set-TimeZone -Id "Central European Standard Time" -PassThru
	Start-Service w32time
	w32tm /resync /force

	Set-LocalUser -Name $User1 -AccountNeverExpires -PasswordNeverExpires 1
	$Password = Read-Host "`nEnter $User2's password" -AsSecureString
	New-LocalUser -Name $User2 -AccountNeverExpires -Password $Password -PasswordNeverExpires
	Add-LocalGroupMember -Group Users -Member $User2
	Rename-Computer -NewName $ComputerName

	Invoke-WebRequest -Uri https://github.com/Sycnex/Windows10Debloater/archive/master.zip -OutFile $env:TEMP\Windows10Debloater.zip
	New-Item -Path $ToolDir -ItemType Directory
	Expand-Archive -Path $env:TEMP\Windows10Debloater.zip -DestinationPath $ToolDir
	Rename-Item -Path $ToolDir\Windows10Debloater-master -NewName Windows10Debloater
	Write-Host "`nEdit 'Windows10Debloater.ps1' file:`n- change `$DebloatFolder to '$ToolDir\Windows10Debloater'" -ForegroundColor Green
	Write-Host "- leave only '.NET|Framework' in `$WhitelistedApp`n- remove '*Nvidia*' from `$NonRemovable`n" -ForegroundColor Green
	Read-Host -Prompt "Press any key to start editing..."
	Start-Process -FilePath notepad.exe -ArgumentList "$ToolDir\Windows10Debloater\Windows10Debloater.ps1" -Wait
	Read-Host -Prompt "Press any key to continue Windows10Debloater..."
	Unblock-File -Path $ToolDir\Windows10Debloater\Windows10Debloater.ps1
	Invoke-Expression -Command $ToolDir\Windows10Debloater\Windows10Debloater.ps1

	$Documents = "0?_*.*"
	foreach ($Files in $Documents)
	{Copy-Item -Path $RepoDir\scripts\$Files -Destination $env:USERPROFILE\Documents -Force}
	$Pictures = "*_admin.*"
	foreach ($Files in $Pictures)
	{Copy-Item -Path $RepoDir\artwork\$Files -Destination $env:USERPROFILE\Pictures -Force}
	$Public = "wsl*.sh","windows*.reg"
	foreach ($Files in $Public)
	{Copy-Item -Path $RepoDir\scripts\$Files -Destination $env:PUBLIC -Force}

	$LanguageList = Get-WinUserLanguageList
	$LanguageList.Add("pl-PL")
	Set-WinUserLanguageList $LanguageList -Force
	Set-WinUserLanguageList -LanguageList pl, en-US -Force

	if ($MyTarget -ne 2) {
		powercfg /hibernate off
		powercfg /devicedisablewake "HID-compliant mouse"

		Enable-WindowsOptionalFeature -NoRestart -Online -FeatureName ServicesForNFS-ClientOnly
		Enable-WindowsOptionalFeature -NoRestart -Online -FeatureName ClientForNFS-Infrastructure
		Enable-WindowsOptionalFeature -NoRestart -Online -FeatureName NFS-Administration
		Enable-WindowsOptionalFeature -NoRestart -Online -FeatureName VirtualMachinePlatform

		Read-Host -Prompt "`nPress any key to hide Input Indicator...`n(Taskbar ---> Turn system icons on or off)"
		Start-Process -FilePath ms-settings:taskbar
		Wait-Process -Name SystemSettings

		Read-Host -Prompt "Press any key to change account picture...`n(Your info ---> Browse for one)"
		Start-Process -FilePath ms-settings:yourinfo
		Wait-Process -Name SystemSettings
	}

	Disable-WindowsOptionalFeature -NoRestart -Online -FeatureName Internet-Explorer-Optional-amd64
	Disable-WindowsOptionalFeature -NoRestart -Online -FeatureName WindowsMediaPlayer
	Disable-WindowsOptionalFeature -NoRestart -Online -FeatureName MediaPlayback
	Enable-WindowsOptionalFeature -NoRestart -Online -FeatureName Microsoft-Windows-Subsystem-Linux

	$QuickAccess = "$env:USERPROFILE\Music\","$env:USERPROFILE\Videos\","C:\","D:\"
	foreach ($Path in $QuickAccess)
	{(New-Object -COMObject Shell.Application).Namespace("$Path").Self.InvokeVerb("pintohome")}

	$Taskbar = "Microsoft Edge"
	((New-Object -COMObject Shell.Application).NameSpace("shell:::{4234d49b-0245-4df3-b780-3893943456e1}").Items() |
	?{$_.Name -eq $Taskbar}).Verbs() | ?{$_.Name.replace("&","") -match "Unpin from taskbar"} | %{$_.DoIt(); $exec = $true}
	$FileExists = Test-Path -Path "$env:PUBLIC\Desktop\Microsoft Edge.lnk"
	if ($FileExists -eq $true) {Remove-Item -Path "$env:PUBLIC\Desktop\Microsoft Edge.lnk" -Force}
	else {Remove-Item -Path "$env:USERPROFILE\Desktop\Microsoft Edge.lnk" -Force}

	taskkill /f /im explorer.exe
	Start-Process -FilePath regedit.exe -ArgumentList "/s","$env:PUBLIC\windows10_HKLM.reg" -Wait
	$WindowsVersion = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion").DisplayVersion
	Set-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate -Name TargetReleaseVersionInfo -Value $WindowsVersion
	Start-Process -FilePath regedit.exe -ArgumentList "/s","$env:PUBLIC\windows10_HKCU.reg" -Wait
	Set-ItemProperty -Path "HKCU:\Control Panel\Desktop" -Name WallPaper -Value C:\Users\admin\Pictures\wallpaper_admin.png
	Set-ItemProperty -Path "HKCU:\Control Panel\Desktop" -Name WallpaperStyle -Value 10

	Get-Process -Name OneDrive -ErrorAction SilentlyContinue | Stop-Process -Force
	Start-Process -FilePath $env:SystemRoot\SysWOW64\OneDriveSetup.exe -ArgumentList "/uninstall" -Wait

	New-Item -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion -Name RunOnce -Force
	Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce -Name NextRun -Value "powershell.exe `"$PSCommandPath -PostReboot`""

	Write-Host "`nAfter reboot log in as $User1 to finish setup." -ForegroundColor Green
	Read-Host -Prompt "Press any key to reboot machine..."
	Restart-Computer
}

function Start-RunAsUser {
	$FileExists = Test-Path -Path $env:PUBLIC\virtual_machine
	if ($FileExists -eq $true) {$MyTarget = 2}
	else {$MyTarget = 1}

	if ($MyTarget -ne 2) {
		curl.exe -L https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi -o $env:TEMP\wsl_update_x64.msi
		Start-Process -FilePath $env:TEMP\wsl_update_x64.msi -Wait
		wsl --set-default-version 2
	}

	curl.exe -L https://github.com/yuk7/AlpineWSL/releases/latest/download/Alpine.zip -o $env:TEMP\Alpine.zip
	Expand-Archive -Path $env:TEMP\Alpine.zip -DestinationPath $env:USERPROFILE\Alpine
	Start-Process -FilePath $env:USERPROFILE\Alpine\Alpine.exe -Wait
	$PublicWSL = wsl wslpath -u `"$env:PUBLIC`"
	Start-Process -FilePath wsl.exe -ArgumentList "sh $PublicWSL/wsl_alpine_setup.sh" -Wait

	Write-Host "`nLog in as $User2 to continue setup." -ForegroundColor Green
	Read-Host -Prompt "Press any key to sign out..."
	shutdown /l
}

if ($PostReboot) {Start-RunAsUser}
else {Start-RunAsAdmin}
