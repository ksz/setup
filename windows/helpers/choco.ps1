if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))
{Start-Process -FilePath powershell.exe -ArgumentList "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit}

if (!(Test-Path -Path $PROFILE))
{New-Item -Path $PROFILE -ItemType File -Force}
Write-Host

Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
Write-Host

choco feature enable --name='allowGlobalConfirmation'
Write-Host

choco feature enable --name='removePackageInformationOnUninstall'
Write-Host

choco feature enable --name='useRememberedArgumentsForUpgrades'
Write-Host

Read-Host -Prompt "End..."


# ============================================================
# https://docs.chocolatey.org/en-us/choco/setup
# https://docs.chocolatey.org/en-us/choco/commands/
# choco list --localonly
# choco pin list
# choco pin add -n=*****
# choco pin remove -n=*****
# choco source list
# choco source add -n=***** -s="**********"
# choco source remove -n=*****
# choco outdated
# choco upgrade all --noop
# choco upgrade all
# choco upgrade all --except="*****,*****"
# choco uninstall ***** -n --skipautouninstaller
# choco uninstall ***** -x -n --skipautouninstaller
# choco pack *****.nuspec
# ============================================================
