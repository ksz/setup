@echo off

set user=admin
set scriptdir=%PUBLIC%\Repo

if "%USERNAME%" == "%user%" (goto RunAsAdmin) else (goto RunAsUser)

:RunAsAdmin
cacls %SystemRoot%\System32\config\SYSTEM 1>nul 2>&1 || (powershell -Command "Start-Process -FilePath '%0' -Verb RunAs" & exit /b)
curl -L https://gitlab.com/ksz/setup/-/archive/main/setup-main.zip?path=windows/helpers -o %TEMP%\repo.zip
powershell -Command "Expand-Archive -Path %TEMP%\repo.zip -DestinationPath %PUBLIC%"
powershell -Command "Rename-Item -Path %PUBLIC%\setup-main-windows-helpers -NewName Repo"
powershell -Command "Move-Item -Path %PUBLIC%\Repo\windows\helpers\* -Destination %PUBLIC%\Repo"
powershell -Command "Remove-Item -Path %PUBLIC%\Repo\windows -Recurse -Force"
powershell -Command "Get-ChildItem -Path %PUBLIC%\Repo\* -Recurse | Unblock-File"
powershell -Command "Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine -Force"
powershell -Command "Start-Process -FilePath powershell.exe -ArgumentList '-NoProfile -ExecutionPolicy Bypass -File %scriptdir%\setup_admin.ps1'"
REM powershell -NoProfile -Command "& {Start-Process -FilePath powershell.exe -ArgumentList '-NoProfile -ExecutionPolicy Bypass -File %scriptdir%\setup_admin.ps1'}"
copy "%~df0" %PUBLIC%\Desktop /y
del %TEMP%\repo.zip
del "%~df0"
goto :eof

:RunAsUser
powershell -Command "Start-Process -FilePath powershell.exe -ArgumentList '-NoProfile -ExecutionPolicy Bypass -File %scriptdir%\setup_user.ps1'"
REM powershell -NoProfile -Command "& {Start-Process -FilePath powershell.exe -ArgumentList '-NoProfile -ExecutionPolicy Bypass -File %scriptdir%\setup_user.ps1'}"
exit
