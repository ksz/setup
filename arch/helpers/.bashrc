#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
# PS1='[\u@\h \W]\$ '

alias ping='ping -c 5'
alias sudo='sudo '
alias wget='wget -c'
alias bc='bc -lq'
alias df='df -h'
alias du='du -ah'
alias ht='htop -t'
alias ka='killall'
alias ll='ls -la'
alias mc='mc -x'
alias nn='nano -l'
alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -Iv'
alias md='mkdir -pv'
alias rd='rmdir -v'
# alias xp='xprop | grep "WM_WINDOW_ROLE\|WM_CLASS" && echo "WM_CLASS(STRING) = \"NAME\", \"CLASS\""'
alias xp='obxprop | grep "^_OB_APP"'
alias or='openbox --reconfigure'
alias ud='update-desktop-database $HOME/.local/share/applications'
alias spu='sudo pacman -Syyu'
alias spi='sudo pacman -S'
alias spr='sudo pacman -Rns'
alias spl='pacman -Qe | grep'
alias yay='yay -a'

PS1='\[\033[01;32m\][\u@\h \[\033[01;34m\]\W\[\033[01;32m\]]\[\033[00m\]\$ '
