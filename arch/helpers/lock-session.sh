#!/bin/sh

username=<USER>
userhome=/home/$username
export XAUTHORITY="$userhome/.Xauthority"
export DISPLAY=":0.0"
# export mydate="$(date +'%Y-%m-%d')"
# export mytime="$(date +'%H:%M:%S')"
case "${1}" in
    pre)
        su $username -c '/usr/local/bin/slock -m "Locked $(date +'%Y-%m-%d') at $(date +'%H:%M:%S')"' &
        # su $username -c '/usr/local/bin/slock -m "Locked $mydate at $mytime"' &
        sleep 1s;
        ;;
esac
