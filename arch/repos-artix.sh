#!/bin/bash

# printf "\n" | sudo tee -a /etc/pacman.conf
# sudo tee -a /etc/pacman.conf <<EOF
# [universe]
# Server = https://universe.artixlinux.org/\$arch
# Server = https://mirror1.artixlinux.org/universe/\$arch
# Server = https://mirror.pascalpuffke.de/artix-universe/\$arch
# Server = https://mirrors.qontinuum.space/artixlinux-universe/\$arch
# Server = https://mirror1.cl.netactuate.com/artix/universe/\$arch
# Server = https://ftp.crifo.org/artix-universe/\$arch
# Server = https://artix.sakamoto.pl/universe/\$arch
# EOF

sudo pacman -S --noconfirm --needed artix-archlinux-support

printf "\n" | sudo tee -a /etc/pacman.conf
sudo tee -a /etc/pacman.conf <<EOF
[extra]
Include = /etc/pacman.d/mirrorlist-arch
EOF

sudo pacman-key --populate archlinux
