#!/bin/bash

printf "\n" | sudo tee -a /etc/pacman.conf
sudo tee -a /etc/pacman.conf <<EOF
[home_maxrd2_Arch]
# Subtitle Composer
SigLevel = Optional TrustAll
Server = https://downloadcontent.opensuse.org/repositories/home:/maxrd2/Arch/x86_64/
EOF

curl -fsSL https://build.opensuse.org/projects/home:maxrd2/signing_keys/download?kind=gpg | sudo pacman-key --add -
sudo pacman-key --lsign-key DE85E73C17AF00C8E865B04F0073ABF073738FA0
