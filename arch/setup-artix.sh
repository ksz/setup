#!/bin/sh

mydotfiles=https://gitlab.com/ksz/dotfiles.git
mysuckless=https://gitlab.com/ksz/suckless.git
mysetup=https://gitlab.com/ksz/setup.git
mydump=https://gitlab.com/ksz/setup/-/raw/main/arch/helpers/disk-*.dump
mypkgs=https://gitlab.com/ksz/setup/-/raw/main/arch/pkgs-*.ini
myrepos=https://gitlab.com/ksz/setup/-/raw/main/arch/repos-artix.sh
myscript=https://gitlab.com/ksz/setup/-/raw/main/arch/setup-artix.sh
myuser=$(grep 1000 /etc/passwd | cut -d \: -f 1)
myhelp="$HOME/.local/src/setup/arch/helpers"
mylog="$HOME/.local/src/setup.log"
mylang=en_US
mytime=Europe/Warsaw
export PATH="$HOME/.local/bin:$PATH"

mymachine=NA
mydewm=NA

function_start () {
    read -p "- Do you want to refresh the setup files? [y/N] " yn
    case "$yn" in
        y) echo "  The files will be refreshed.";
           function_refresh ;;
        *) echo "  The files remain unchanged.";
           function_user1 ;;
    esac
}

function_refresh () {
    touch /tmp/myrefresh
    curl -sLo ~/setup.sh "$myscript"
    read -p "- Do you want to edit the setup script before installation? [y/N] " yn
    case "$yn" in
        y) nano -l ~/setup.sh ;;
        *) echo "  The setup script remains unchanged." ;;
    esac
    sh ~/setup.sh -s
}

function_select () {
    echo "- Choose machine:"
    echo "  1) phys 2) qemu 3) vbox"
    while true; do
        read -p "  Enter a number: " mymachine
        case "$mymachine" in
            1) mymachine=phys && mydisk=nvme0n1; break ;;
            2) mymachine=qemu && mydisk=vda; break ;;
            3) mymachine=vbox && mydisk=sda; break ;;
            *) echo "  Invalid input - try again."
        esac
    done
    if [ "$mymachine" = "phys" ]; then
        echo "- Choose hostname:"
        echo "  1) op7050 2) k52jc 3) other"
        while true; do
            read -p "  Enter a number: " myhost
            case "$myhost" in
                1) myhost=OP7050; break ;;
                2) myhost=K52JC; break ;;
                3) read -p "- Type in a hostname: " myhost;
                   echo "  Hostname has been changed."; break ;;
                *) echo "  Invalid input - try again."
            esac
        done
    fi
    echo "- Choose DE/WM:"
    echo "  1) lxqt 2) xfce 3) dwm"
    while true; do
        read -p "  Enter a number: " mydewm
        case "$mydewm" in
            1) mydewm=lxqt; break ;;
            2) mydewm=xfce; break ;;
            3) mydewm=dwm; break ;;
            *) echo "  Invalid input - try again."
        esac
    done
    sed -i "/^mymachine=NA$/s/NA/$mymachine/;/^mydewm=NA$/s/NA/$mydewm/" ~/setup.sh
    mypkgs=$(echo "$mypkgs" | sed "s/\*/$mydewm/")
    curl -sLo ~/pkgs.ini "$mypkgs"
    curl -sLo ~/repos.sh "$myrepos"
    read -p "- Do you want to edit the list of packages before installation? [y/N] " yn
    case "$yn" in
        y) nano -l ~/pkgs.ini ;;
        *) echo "  The list of packages remains unchanged." ;;
    esac
    read -p "- Do you want to edit the list of repositories before installation? [y/N] " yn
    case "$yn" in
        y) nano -l ~/repos.sh ;;
        *) echo "  The list of repositories remains unchanged." ;;
    esac
    if test -f /tmp/myrefresh; then
        sh ~/setup.sh -u
    else
        function_install
    fi
}

function_install () {
    echo -n "- Enter root password: "
    read -s rootpass1 && printf "\n"
    echo -n "  Retype password: "
    read -s rootpass2 && printf "\n"
    while ! [ "$rootpass1" = "$rootpass2" ] ; do
        unset rootpass1 rootpass2
        echo -n "  Passwords do not match. Enter password again: "
        read -s rootpass1 && printf "\n"
        echo -n "  Retype password: "
        read -s rootpass2 && printf "\n"
    done
    read -p "- Type in a new user name: " myuser
    echo "  User $myuser added."
    echo -n "- Enter $myuser password: "
    read -s userpass1 && printf "\n"
    echo -n "  Retype password: "
    read -s userpass2 && printf "\n"
    while ! [ "$userpass1" = "$userpass2" ] ; do
        unset userpass1 userpass2
        echo -n "  Passwords do not match. Enter password again: "
        read -s userpass1 && printf "\n"
        echo -n "  Retype password: "
        read -s userpass2 && printf "\n"
    done
    echo
    if [ "$mymachine" = phys ]; then
        # mkfs.fat -F 32 /dev/"$mydisk"p1                                                                 # > GParted
        # mkfs.ext4 -F /dev/"$mydisk"p2                                                                   # > GParted
        # mkfs.ext4 -F /dev/"$mydisk"p3                                                                   # > GParted
        # # mkswap /dev/"$mydisk"p4                                                                       # > GParted
        mount /dev/"$mydisk"p2 /mnt
        mount --mkdir /dev/"$mydisk"p1 /mnt/boot
        # mount --mkdir /dev/"$mydisk"p3 /mnt/mnt/DATA
        # # swapon /dev/"$mydisk"p4
    else
        myhost=VIRT
        mydump=$(echo "$mydump" | sed "s/\*/$mydisk/")
        curl -sLo ~/disk.dump "$mydump"
        sfdisk /dev/$mydisk < ~/disk.dump
        mkfs.ext4 -L ROOT /dev/"$mydisk"1
        mkswap -L SWAP /dev/"$mydisk"2
        mount /dev/disk/by-label/ROOT /mnt
        swapon /dev/disk/by-label/SWAP
    fi
    ntpdate pool.ntp.org
    basestrap /mnt base base-devel linux nano runit connman-runit elogind-runit
    fstabgen -U /mnt >> /mnt/etc/fstab
    artix-chroot /mnt <<EOF
ln -sf /usr/share/zoneinfo/$mytime /etc/localtime
hwclock --systohc
sed -i "/^#$mylang/s/^#//g" /etc/locale.gen
locale-gen
echo "LANG=$mylang.UTF-8" > /etc/locale.conf
echo "$myhost" > /etc/hostname
echo "127.0.0.1    localhost" >> /etc/hosts
echo "::1          localhost" >> /etc/hosts
echo "127.0.1.1    $myhost" >> /etc/hosts
echo "root:$rootpass1" | chpasswd
useradd -m -G wheel "$myuser"
echo "$myuser:$userpass1" | chpasswd
echo "%wheel ALL=(ALL:ALL) ALL" > /etc/sudoers.d/00-wheel-all
echo "%wheel ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/01-no-password
echo "Defaults timestamp_timeout=15" > /etc/sudoers.d/02-timeout
if [ "$mymachine" = phys ]; then
    # pacman -S --noconfirm --needed gptfdisk                                                             # > GParted
    # sgdisk -i 1 /dev/$mydisk | grep -q "EFI system partition" || sgdisk -t 1:ef00 /dev/$mydisk          # > GParted
    pacman -S --noconfirm --needed grub efibootmgr intel-ucode os-prober
    grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
else
    pacman -S --noconfirm --needed grub os-prober
    grub-install --target=i386-pc /dev/$mydisk
fi
cp /etc/default/grub /etc/default/grub.bak
sed -i "/GRUB_TIMEOUT=/s/5/2/;/^#GRUB_DISABLE_OS_PROBER=false/s/^#//" /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg
ln -s /etc/runit/sv/connmand /etc/runit/runsvdir/default
EOF
    cp ~/pkgs.ini ~/repos.sh ~/setup.sh /mnt/home/$myuser
    chmod 666 /mnt/home/$myuser/pkgs.ini /mnt/home/$myuser/repos.sh /mnt/home/$myuser/setup.sh
    rm -f ~/disk.dump ~/pkgs.ini ~/repos.sh ~/setup.sh
    umount -R /mnt
    if [ "$mymachine" = vbox ]; then
        echo -e "\e[32m  Power off your computer, eject an installation medium and take a snapshot."
    else
        echo -e "\e[32m  Reboot your computer."
    fi
    echo -e "\e[32m  Then log in as\e[37m \"$myuser\"\e[32m and run\e[37m \"sh setup.sh\"\e[32m.\e[0m"
}

function_user1 () {
    echo
    sudo sv restart connmand
    sudo sh -c ":> /root/.bash_history"
    mkdir -p ~/.local/bin ~/.local/src && echo -e "$(date)\n" > "$mylog"
    sudo cp /etc/pacman.conf /etc/pacman.conf.bak && sudo cp /etc/makepkg.conf /etc/makepkg.conf.bak
    grep -q ILoveCandy /etc/pacman.conf || sudo sed -i "/#VerbosePkgLists/a ILoveCandy" /etc/pacman.conf
    sudo sed -Ei "s/^#(ParallelDownloads).*/\1 = 5/;/^#Color$/s/^#//" /etc/pacman.conf
    sudo sed -i "s/-j2/-j$(nproc)/;/^#MAKEFLAGS/s/^#//" /etc/makepkg.conf
    function_user2 2>&1 | tee -a "$mylog"
}

function_user2 () {
    sudo pacman -Sy --noconfirm --needed artix-keyring
    bash ~/repos.sh
    sudo pacman -Syyu --noconfirm --needed
    grep -E -v '^; |^A |^D |^G |^$' ~/pkgs.ini | xargs sudo pacman -S --noconfirm --needed
    if [ $? -ne 0 ]; then
        grep -E -v '^; |^A |^D |^G |^$' ~/pkgs.ini | xargs -n 1 sudo pacman -S --noconfirm --needed
    fi
    grep -E '^D ' ~/pkgs.ini | cut -c 3- | while read line; do sh -c "$line"; done
    git clone --depth 1 https://aur.archlinux.org/yay-bin.git ~/.local/src/aur
    git clone --depth 1 "$mydotfiles" ~/.local/src/dotfiles
    git clone --depth 1 "$mysetup" ~/.local/src/setup
    git clone --depth 1 "$mysuckless" ~/.local/src/suckless
    cd ~/.local/src/aur
    makepkg -si --noconfirm
    cd ~/.local/src/suckless
    grep -E '^G ' ~/pkgs.ini | cut -d ' ' -f 2 | while read pkg; do cd "$pkg"; sudo make clean install; cd ..; done
    cd ~
    grep -E '^A ' ~/pkgs.ini | cut -d ' ' -f 2 | xargs yay -S --noconfirm --needed
    sudo pacman -S --noconfirm --needed rpcbind-runit
    sudo ln -s /etc/runit/sv/rpcbind /run/runit/service
    sudo pacman -Rns --noconfirm $(pacman -Qtdq)
    find ~/.local/src/dotfiles -type f ! -name "setup-*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser/" "{}" \;
    find ~/.local/src/setup -type f ! -name "setup*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser/" "{}" \;
    # xdg-user-dirs-update
    # ln -s $myhelp/artix-wallpaper.jpg $(xdg-user-dir PICTURES)/wallpaper.jpg
    # ln -s $myhelp/artix-wallpaper-rotate.jpg $(xdg-user-dir PICTURES)/wallpaper-rotate.jpg
    # if [ "$mydewm" = "dwm" ]; then
    #     rsync -r ~/.local/src/dotfiles/dwm/ ~/.
    #     rsync -r ~/.local/src/dotfiles/kde/ ~/.     # ### KDE/Openbox ### #
    #     sudo chsh -s /bin/zsh "$myuser"
    # elif [ "$mydewm" = "lxqt" ]; then
    #     rsync -r ~/.local/src/dotfiles/lxqt/ ~/.
    #     sudo pacman -S --noconfirm --needed sddm-runit
    #     sudo ln -s /etc/runit/sv/sddm /run/runit/service
    #     (sleep 15; sudo sv stop sddm) &
    # else
    #     export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$(id -u "$myuser")/bus
    #     export DISPLAY=:0.0
    #     rsync -r ~/.local/src/dotfiles/xfce/ ~/.
    #     sudo pacman -S --noconfirm --needed lightdm-runit
    #     sudo ln -s /etc/runit/sv/lightdm /run/runit/service
    #     grep -E -v '^# |^$' $myhelp/backup-xfconf | while IFS=';' read i j k l; do xfconf-query -c $i -np $j -t $k -s "$l"; done
    #     grep -E -v '^# |^$' $myhelp/backup-gsettings | while IFS=' ' read i j k; do gsettings set $i $j "$k"; done
    #     xfconf-query -c xfce4-desktop -np $(xfconf-query -c xfce4-desktop -l | grep workspace0/last-image) -s $(xdg-user-dir PICTURES)/wallpaper.jpg
    # fi
    rsync -r ~/.local/src/dotfiles/$mydewm/ ~/.
    if [ "$mymachine" = "phys" ]; then
        sudo pacman -S --noconfirm --needed linux-firmware ntp-runit # mesa
        sudo ln -s /etc/runit/sv/ntpd /run/runit/service
        test -d /etc/elogind/system-sleep || sudo mkdir /etc/elogind/system-sleep
        sudo cp $myhelp/lock.sh $myhelp/ssd-freeze.sh /etc/elogind/system-sleep
        sudo chmod +x /etc/elogind/system-sleep/*.sh
        sudo dd if=/dev/zero of=/swapfile bs=1M count=4k status=progress
        sudo chmod 600 /swapfile && sudo mkswap -U clear /swapfile && sudo swapon /swapfile
        echo "/swapfile none swap defaults 0 0" | sudo tee -a /etc/fstab
        test -d /etc/sysctl.d || sudo mkdir /etc/sysctl.d
        echo "vm.swappiness = 10" | sudo tee /etc/sysctl.d/99-swappiness.conf
        test -f ~/.xinitrc && sed -i "/video-toggle.sh -1/s/^# //" ~/.xinitrc
    fi
    if [ "$mymachine" = "phys" ] || [ "$mymachine" = "qemu" ]; then
        sudo pacman -S --noconfirm --needed cronie-runit
        sudo ln -s /etc/runit/sv/cronie /run/runit/service
        # todo: fstrim
    fi
    if [ "$mymachine" = "qemu" ]; then
        sudo pacman -S --noconfirm --needed dropbear-runit spice-vdagent-runit
        sudo ln -s /etc/runit/sv/dropbear /run/runit/service
        sudo ln -s /etc/runit/sv/spice-vdagentd /run/runit/service
        test -f ~/.xinitrc && sed -i "/spice-vdagent/s/^# //;/video-toggle.sh -q/s/^# //" ~/.xinitrc
    fi
    if [ "$mymachine" = "qemu" ] || [ "$mymachine" = "vbox" ]; then
        test -f ~/.xinitrc && sed -i "/idle-detect.sh -x/s/^/# /" ~/.xinitrc
    fi
    if [ "$mymachine" = "vbox" ]; then
        sudo pacman -S --noconfirm --needed virtualbox-guest-iso linux-headers
        sudo mount /usr/lib/virtualbox/additions/VBoxGuestAdditions.iso /mnt
        sudo sh /mnt/VBoxLinuxAdditions.run --nox11
        # sudo pacman -S --noconfirm --needed virtualbox-guest-utils linux-headers
        # # sudo modprobe vboxsf
        # # sudo mount -t vboxsf <share> /media/sf_<share>
        sudo usermod -a -G vboxsf "$myuser"
        test -f ~/.xinitrc && sed -i "/VBoxClient-all/s/^# //;/video-toggle.sh -v/s/^# //" ~/.xinitrc
    fi
    if which lightdm | grep -q lightdm; then
        sudo cp /etc/lightdm/lightdm-gtk-greeter.conf /etc/lightdm/lightdm-gtk-greeter.conf.bak
        sudo tee /etc/lightdm/lightdm-gtk-greeter.conf <<EOF
[greeter]
# background=/usr/share/backgrounds/mate/desktop/Float-into-MATE.png
# background=/usr/share/backgrounds/xfce/xfce-shapes.svg
theme-name = Arc-Dark
icon-theme-name = Papirus-Dark
font-name = Cantarell 9
indicators = ~host;~spacer;~session;~clock;~power
EOF
        sudo pacman -S --noconfirm --needed lightdm-runit lightdm-gtk-greeter
        sudo ln -s /etc/runit/sv/lightdm /run/runit/service
        # todo: systemctl --user enable conf-set.service
    fi
    if which sddm | grep -q sddm; then
        sudo mkdir /etc/sddm.conf.d
        sudo tee /etc/sddm.conf.d/sddm.conf <<EOF
[Autologin]
#User=
#Session=lxqt.desktop
[General]
InputMethod=
[Theme]
Current=catppuccin-frappe
ThemeDir=/usr/share/sddm/themes
[X11]
#DisplayCommand=/etc/sddm/Xsetup
#DisplayStopCommand=/etc/sddm/Xstop
EOF
        sudo pacman -S --noconfirm --needed sddm-runit
        sudo ln -s /etc/runit/sv/sddm /run/runit/service
        (sleep 15; sudo sv stop sddm) &
    fi
    test -d /var/lib/dbus || sudo mkdir /var/lib/dbus
    sudo sh -c "dbus-uuidgen  >| /etc/machine-id" && sudo ln -sf /etc/machine-id /var/lib/dbus/machine-id
    # find ~/.local/bin -type f -exec chmod +x "{}" \;
    bash post-install.sh arch artix
    sudo os-prober && sudo grub-mkconfig -o /boot/grub/grub.cfg
    echo "%wheel ALL=(ALL:ALL) NOPASSWD: /bin/grub-reboot,/bin/mount,/bin/umount,\
/bin/pacman -Syu,/bin/pacman -Syyu,/bin/pacman -Syy" | sudo tee /etc/sudoers.d/01-no-password
    rm -f ~/pkgs.ini ~/repos.sh ~/setup.sh
    echo -e "\e[32m  Operation successfully completed.\n  Reboot your computer and log in as\e[37m \"$myuser\"\e[32m.\e[0m"
}

case $1 in
    -s) function_select ;;
    -u) function_user1 ;;
    * ) function_start ;;
esac
