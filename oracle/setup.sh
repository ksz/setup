#!/bin/bash

# Oracle Linux 9

mydotfiles=https://gitlab.com/ksz/dotfiles.git
# mysetup=https://gitlab.com/ksz/setup.git
mypkgs=https://gitlab.com/ksz/setup/-/raw/main/oracle/pkgs.ini
myrepos=https://gitlab.com/ksz/setup/-/raw/main/oracle/repos.sh
mylog="$HOME/.local/src/setup.log"

function_select () {
    echo "- Choose machine:"
    echo "  1) qemu 2) vbox 3) vps"
    while true; do
        read -p "  Enter a number: " mymachine
        case "$mymachine" in
            1) mymachine=qemu; break ;;
            2) mymachine=vbox; break ;;
            3) mymachine=vps; break ;;
            *) echo "  Invalid input - try again."
        esac
    done
    if [ "$mymachine" = "vps" ]; then
        sudo passwd
        sudo passwd $(whoami)
    fi
    curl -sLo ~/pkgs.ini "$mypkgs"
    curl -sLo ~/repos.sh "$myrepos"
    read -p "- Do you want to edit the list of packages before installation? [y/N] " yn
    case "$yn" in
        y) nano -l ~/pkgs.ini ;;
        *) echo "  The list of packages remains unchanged." ;;
    esac
    read -p "- Do you want to edit the list of repositories before installation? [y/N] " yn
    case "$yn" in
        y) nano -l ~/repos.sh ;;
        *) echo "  The list of repositories remains unchanged." ;;
    esac
    mkdir -p ~/.local/src && echo -e "$(date)\n" > "$mylog"
    echo
    function_install 2>&1 | tee -a "$mylog"
}

function_install () {
    if [ "$mymachine" != "vps" ]; then
        echo "%wheel ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/01-no-password
        echo "Defaults timestamp_timeout=15" | sudo tee /etc/sudoers.d/02-timeout
        sudo hostnamectl set-hostname VIRT
        sudo systemctl enable fstrim.timer
        sudo systemctl start fstrim.timer
        sudo cp /etc/default/grub /etc/default/grub.bak
        sudo sed -i "/GRUB_TIMEOUT=/s/5/2/" /etc/default/grub
        sudo grub2-mkconfig -o /boot/grub2/grub.cfg
    fi
    sudo cp /etc/dnf/dnf.conf /etc/dnf/dnf.conf.bak
    sudo tee -a /etc/dnf/dnf.conf <<EOF
max_parallel_downloads=10
fastestmirror=True
defaultyes=True
installonly_limit=2
EOF
    bash ~/repos.sh
    sudo dnf -y upgrade --refresh
    grep -E -v '^; |^D |^$' ~/pkgs.ini | xargs sudo dnf -y install
    if [ $? -ne 0 ]; then grep -E -v '^; |^D |^$' ~/pkgs.ini | xargs -n 1 sudo dnf -y install; fi
    grep -E '^D ' ~/pkgs.ini | cut -c 3- | while read line; do sh -c "$line"; done
    for i in 80 443; do sudo firewall-cmd --zone=public --add-port=${i}/tcp --permanent; done
    sudo firewall-cmd --reload
    echo "export EDITOR=/bin/nano" | sudo tee -a /etc/environment
    git clone --depth 1 "$mydotfiles" ~/.local/src/dotfiles
    # git clone --depth 1 "$mysetup" ~/.local/src/setup
    rsync -r ~/.local/src/dotfiles/nogui/ ~/.
    find ~/.local/bin -type f -exec chmod +x "{}" \;
    # cd ~/docker && sudo docker compose up -d
    # sleep 10
    rm -rf ~/.local/src/dotfiles ~/pkgs.ini ~/repos.sh ~/setup.sh
    if [ "$mymachine" != "vps" ]; then
        echo "%wheel ALL=(ALL) NOPASSWD: /bin/mount,/bin/umount" | sudo tee /etc/sudoers.d/01-no-password
        reboot
    else
        sudo reboot
    fi
}

case $1 in
    -i) function_install ;;
    * ) function_select ;;
esac
