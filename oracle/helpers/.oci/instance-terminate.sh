#!/bin/bash

read -p "Enter the name of the instance: " display_name
compartment_id=$(grep tenancy "$HOME/.oci/config" | sed "s/tenancy=//")
instance_id=$(oci compute instance list --compartment-id "${compartment_id}" --query "data[?\"display-name\"=='${display_name}' && \"lifecycle-state\"=='RUNNING'].id" --output json | jq -r '.[0]')
instance_dn=$(oci compute instance get --instance-id "${instance_id}" --query 'data."display-name"' --output json | jq -r '.')
vcn_id=$(oci network vcn list --compartment-id "${compartment_id}" --query "data[?\"display-name\"=='${display_name}'].id" --output json | jq -r '.[0]')
vcn_dn=$(oci network vcn get --vcn-id "${vcn_id}" --query 'data."display-name"' --output json | jq -r '.')
subnet_ids=$(oci network subnet list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query 'data[*].id' --output json | jq -r '.[]')
subnet_dns=$(oci network subnet list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query 'data[*]."display-name"' --output json | jq -r '.[]')
route_table_ids=$(oci network route-table list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query 'data[*].id' --output json | jq -r '.[]')
route_table_dns=$(oci network route-table list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query 'data[*]."display-name"' --output json | jq -r '.[]')
route_table_private_id=$(oci network route-table list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query "data[?contains(\"display-name\", 'private')].id" --output json | jq -r '.[]')
route_table_private_dn=$(oci network route-table get --rt-id "${route_table_private_id}" --query 'data."display-name"' --output json | jq -r '.')
# security_list_ids=$(oci network security-list list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query 'data[*].id' --output json | jq -r '.[]')
# security_list_dns=$(oci network security-list list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query 'data[*]."display-name"' --output json | jq -r '.[]')
security_list_private_id=$(oci network security-list list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query "data[?contains(\"display-name\", 'private')].id" --output json | jq -r '.[]')
security_list_private_dn=$(oci network security-list get --security-list-id "${security_list_private_id}" --query 'data."display-name"' --output json | jq -r '.')
internet_gateway_id=$(oci network internet-gateway list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query 'data[0].id' --output json | jq -r '.')
internet_gateway_dn=$(oci network internet-gateway list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query 'data[0]."display-name"' --output json | jq -r '.')
nat_gateway_id=$(oci network nat-gateway list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query 'data[0].id' --output json | jq -r '.')
nat_gateway_dn=$(oci network nat-gateway list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query 'data[0]."display-name"' --output json | jq -r '.')
service_gateway_id=$(oci network service-gateway list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query 'data[0].id' --output json | jq -r '.')
service_gateway_dn=$(oci network service-gateway list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" --query 'data[0]."display-name"' --output json | jq -r '.')

echo -e "\nTerminating Instance ..."
oci compute instance terminate \
    --instance-id "${instance_id}" \
    --force \
    --wait-for-state "TERMINATED" 2> /dev/null
echo "Terminated Instance: "${instance_dn}""

echo -e "\nClearing Route Rules from Route Tables ..."
echo "${route_table_ids}" | while read -r route_table_id; do
    oci network route-table update \
        --rt-id "${route_table_id}" \
        --route-rules "[]" \
        --force \
        --wait-for-state "AVAILABLE" 2> /dev/null
done
echo "${route_table_dns}" | while read -r route_table_dn; do
    echo "Cleared Route Rules from Route Table: "${route_table_dn}""
done

echo -e "\nDeleting Internet Gateway ..."
oci network internet-gateway delete \
    --ig-id "${internet_gateway_id}" \
    --force \
    --wait-for-state "TERMINATED" 2> /dev/null
echo "Deleted Internet Gateway: "${internet_gateway_dn}""

echo -e "\nDeleting NAT Gateway ..."
oci network nat-gateway delete \
    --nat-gateway-id "${nat_gateway_id}" \
    --force \
    --wait-for-state "TERMINATED" 2> /dev/null
echo "Deleted NAT Gateway: "${nat_gateway_dn}""

echo -e "\nDeleting Service Gateway ..."
oci network service-gateway delete \
    --service-gateway-id "${service_gateway_id}" \
    --force \
    --wait-for-state "TERMINATED" 2> /dev/null
echo "Deleted Service Gateway: "${service_gateway_dn}""

echo -e "\nDeleting Subnets ..."
echo "${subnet_ids}" | while read -r subnet_id; do
    oci network subnet delete \
        --subnet-id "${subnet_id}" \
        --force \
        --wait-for-state "TERMINATED" 2> /dev/null
done
echo "${subnet_dns}" | while read -r subnet_dn; do
    echo "Deleted Subnet: "${subnet_dn}""
done

### can not delete default route table
echo -e "\nDeleting Private Route Table ..."
oci network route-table delete \
    --rt-id "${route_table_private_id}" \
    --force \
    --wait-for-state "TERMINATED" 2> /dev/null
echo "Deleted Private Route Table: "${route_table_private_dn}""

### can not delete default security list
echo -e "\nDeleting Private Security List ..."
oci network security-list delete \
    --security-list-id "${security_list_private_id}" \
    --force \
    --wait-for-state "TERMINATED" 2> /dev/null
echo "Deleted Private Security List: "${security_list_private_dn}""

echo -e "\nDeleting VCN ..."
oci network vcn delete \
    --vcn-id "${vcn_id}" \
    --force \
    --wait-for-state "TERMINATED" 2> /dev/null
echo "Deleted VCN: "${vcn_dn}""
