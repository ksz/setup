#!/bin/bash

read -p "Enter the name of the instance: " display_name
compartment_id=$(grep tenancy "$HOME/.oci/config" | sed "s/tenancy=//")
instance_id=$(oci compute instance list \
    --compartment-id "${compartment_id}" \
    --query "data[?\"display-name\"=='${display_name}' && \"lifecycle-state\"=='RUNNING'].id" \
    --output json | jq -r '.[0]')
vcn_id=$(oci network vcn list \
    --compartment-id "${compartment_id}" \
    --query "data[?\"display-name\"=='${display_name}'].id" \
    --output json | jq -r '.[0]')
vnic_id=$(oci compute instance list-vnics \
    --instance-id "${instance_id}" \
    --query 'data[].id' \
    --output json | jq -r '.[0]')
config_file="$HOME/.oci/$display_name.json"

echo "INSTANCE" > "$config_file"
oci compute instance get --instance-id "${instance_id}" | jq -r .data >> "$config_file"

echo -e "\nVCN" >> "$config_file"
oci network vcn get --vcn-id "${vcn_id}" | jq -r .data >> "$config_file"

echo -e "\nSUBNET" >> "$config_file"
oci network subnet list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" | jq -r '.data[]' >> "$config_file"

echo -e "\nROUTE_TABLE" >> "$config_file"
oci network route-table list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" | jq -r '.data[]' >> "$config_file"

echo -e "\nSECURITY_LIST" >> "$config_file"
oci network security-list list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" | jq -r '.data[]' >> "$config_file"

echo -e "\nINTERNET_GATEWAY" >> "$config_file"
oci network internet-gateway list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" | jq -r '.data[]' >> "$config_file"

echo -e "\nNAT_GATEWAY" >> "$config_file"
oci network nat-gateway list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" | jq -r '.data[]' >> "$config_file"

echo -e "\nSERVICE_GATEWAY" >> "$config_file"
oci network service-gateway list --compartment-id "${compartment_id}" --vcn-id "${vcn_id}" | jq -r '.data[]' >> "$config_file"

echo -e "\nVNIC" >> "$config_file"
oci compute instance list-vnics --instance-id "${instance_id}" | jq -r '.data[]' >> "$config_file"
oci compute vnic-attachment list --compartment-id "${compartment_id}" --instance-id "${instance_id}" | jq -r '.data[]' >> "$config_file"

echo -e "\nPRIVATE_IP" >> "$config_file"
oci network private-ip list --vnic-id "${vnic_id}" --all | jq -r '.data[]' >> "$config_file"

echo -e "\nPUBLIC_IP" >> "$config_file"
oci network public-ip list --compartment-id "${compartment_id}" --scope REGION --all | jq -r '.data[]' >> "$config_file"

echo -e "\nBOOT_VOLUME" >> "$config_file"
oci bv boot-volume list --compartment-id "${compartment_id}" | jq -r '.data[]' >> "$config_file"
