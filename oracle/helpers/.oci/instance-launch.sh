#!/bin/bash

compartment_id=$(grep tenancy "$HOME/.oci/config" | sed "s/tenancy=//")
key_file=$(grep key_file "$HOME/.oci/config" | sed "s/key_file=//")
today=$(date +%Y%m%d)
display_name="VPS-$today"
ip_reserved=***.***.***.***
ip_reserved_dn=ReservedPublicIP
port_list="80 443"
vcn_dns_label="vcn$today"
subnet_public_dns_label="subnet${today}1"
subnet_private_dns_label="subnet${today}2"
vcn_cidr_block=10.0.0.0/16
subnet_public_cidr_block=10.0.0.0/24
subnet_private_cidr_block=10.0.1.0/24
operating_system="Oracle Linux"
shape=VM.Standard.A1.Flex
availability_domain=LnXQ:EU-FRANKFURT-1-AD-1
boot_volume_size_in_gbs=50
memory_in_gbs=6.0
ocpus=1.0
vcpus=1
ssh_authorized_keys_file="$HOME/.oci/id_ed25519*.pub"

if ! test -f "$key_file"; then
    echo "Copy \"$(basename $key_file)\" to \"$(pwd)\""
    read -p "Press [Enter] key to continue..."
fi

chmod 600 "$HOME"/.oci/{config,*.pem,*.pub}
sed -i "/$ip_reserved/d" "$HOME/.ssh/known_hosts"

if which oci >/dev/null 2>&1; then
    echo -e "\nPackage 'oci-cli' is already installed."
else
    sudo dnf -y install oci-cli
fi

image_id=$(oci compute image list \
    --compartment-id "${compartment_id}" \
    --operating-system "${operating_system}" \
    --shape "${shape}" \
    --query 'data[0].id' \
    --output json | jq -r '.')
image_dn=$(oci compute image get \
    --image-id "${image_id}" \
    --query 'data."display-name"' \
    --output json | jq -r '.')
service_id=$(oci network service list \
    --all \
    --query "data[?name=='All FRA Services In Oracle Services Network'].id" \
    --output json | jq -r '.[]')

cat <<EOF

Parameters of the new instance:
- Name........$display_name
- OS..........$image_dn
- RAM.........$memory_in_gbs
- CPU.........$ocpus

EOF
read -p "Press [Enter] key to continue..."

echo -e "\nCreating VCN ..."
vcn=$(oci network vcn create \
    --compartment-id "${compartment_id}" \
    --display-name "${display_name}" \
    --cidr-block "${vcn_cidr_block}" \
    --dns-label "${vcn_dns_label}" \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
vcn_id=$(echo "${vcn}" | jq -r '.id')
echo "Created VCN: $(echo "${vcn}" | jq -r '."display-name"')"

echo -e "\nCreating Subnet (public) ..."
subnet=$(oci network subnet create \
    --compartment-id "${compartment_id}" \
    --display-name "public subnet-${display_name}" \
    --cidr-block "${subnet_public_cidr_block}" \
    --dns-label "${subnet_public_dns_label}" \
    --vcn-id "${vcn_id}" \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
subnet_public_id=$(echo "${subnet}" | jq -r '.id')
echo "Created Public Subnet: $(echo "${subnet}" | jq -r '."display-name"')"

echo -e "\nCreating Subnet (private) ..."
subnet=$(oci network subnet create \
    --compartment-id "${compartment_id}" \
    --display-name "private subnet-${display_name}" \
    --cidr-block "${subnet_private_cidr_block}" \
    --dns-label "${subnet_private_dns_label}" \
    --prohibit-internet-ingress true \
    --prohibit-public-ip-on-vnic true \
    --vcn-id "${vcn_id}" \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
subnet_private_id=$(echo "${subnet}" | jq -r '.id')
echo "Created Private Subnet: $(echo "${subnet}" | jq -r '."display-name"')"

echo -e "\nCreating Internet Gateway ..."
internet_gateway=$(oci network internet-gateway create \
    --compartment-id "${compartment_id}" \
    --display-name "Internet gateway-${display_name}" \
    --vcn-id "${vcn_id}" \
    --is-enabled true \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
internet_gateway_id=$(echo "${internet_gateway}" | jq -r '.id')
echo "Created Internet Gateway: $(echo "${internet_gateway}" | jq -r '."display-name"')"

echo -e "\nCreating NAT Gateway ..."
nat_gateway=$(oci network nat-gateway create \
    --compartment-id "${compartment_id}" \
    --display-name "NAT gateway-${display_name}" \
    --vcn-id "${vcn_id}" \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
nat_gateway_id=$(echo "${nat_gateway}" | jq -r '.id')
echo "Created NAT Gateway: $(echo "${nat_gateway}" | jq -r '."display-name"')"

echo -e "\nCreating Service Gateway ..."
service_gateway=$(oci network service-gateway create \
    --compartment-id "${compartment_id}" \
    --display-name "Service gateway-${display_name}" \
    --vcn-id "${vcn_id}" \
    --services='[
        {"service-id": "'${service_id}'",
        "service-name": "All FRA Services In Oracle Services Network"}
    ]' \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
service_gateway_id=$(echo "${service_gateway}" | jq -r '.id')
echo "Created Service Gateway: $(echo "${service_gateway}" | jq -r '."display-name"')"

echo -e "\nUpdating Route Rules in Route Table (default) ..."
route_table_default_id=$(echo "${vcn}" | jq -r '.["default-route-table-id"]')
route_table=$(oci network route-table get \
    --rt-id "${route_table_default_id}" \
    | jq -rc '.data')
route_rules=$(echo "${route_table}" | jq -rc '.["route-rules"]')
route_rule=$(echo '{
    "destination":"0.0.0.0/0",
    "destinationType":"CIDR_BLOCK",
    "networkEntityId":"'${internet_gateway_id}'"
}' | jq -rc)
route_rules=$(echo "${route_rules}" | jq -rc --arg route_rule "${route_rule}" '. + [ $route_rule | fromjson ]')
route_table=$(oci network route-table update \
    --rt-id "${route_table_default_id}" \
    --display-name "default route table for ${display_name}" \
    --route-rules "${route_rules}" \
    --force \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
route_rules=$(echo "${route_table}" | jq -rc '.["route-rules"]')
echo "Updated Route Rules in Route Table (default): $(echo "${route_table}" | jq -r '."display-name"')"

echo -e "\nCreating Route Table (private) ..."
route_table=$(oci network route-table create \
    --compartment-id "${compartment_id}" \
    --display-name "route table for private subnet-${display_name}" \
    --route-rules "[]" \
    --vcn-id "${vcn_id}" \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
route_table_private_id=$(echo "${route_table}" | jq -r '.id')
echo "Created Route Table (private): $(echo "${route_table}" | jq -r '."display-name"')"

echo -e "\nUpdating Route Rules in Route Table (private) ..."
route_rules=$(echo "${route_table}" | jq -rc '.["route-rules"]')
route_rule=$(echo '{
    "destination":"0.0.0.0/0",
    "destinationType":"CIDR_BLOCK",
    "networkEntityId":"'${nat_gateway_id}'"
}' | jq -rc)
route_rules=$(echo "${route_rules}" | jq -rc --arg route_rule "${route_rule}" '. + [ $route_rule | fromjson ]')
route_rule=$(echo '{
    "destination":"all-fra-services-in-oracle-services-network",
    "destinationType":"SERVICE_CIDR_BLOCK",
    "networkEntityId":"'${service_gateway_id}'"
}' | jq -rc)
route_rules=$(echo "${route_rules}" | jq -rc --arg route_rule "${route_rule}" '. + [ $route_rule | fromjson ]')
route_table=$(oci network route-table update \
    --rt-id "${route_table_private_id}" \
    --route-rules "${route_rules}" \
    --force \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
route_rules=$(echo "${route_table}" | jq -rc '.["route-rules"]')
echo "Updated Route Rules in Route Table (private): $(echo "${route_table}" | jq -r '."display-name"')"

echo -e "\nUpdating Ingress Rules in Security List (default) ..."
security_list_default_id=$(echo "${vcn}" | jq -r '.["default-security-list-id"]')
security_list=$(oci network security-list get \
    --security-list-id "${security_list_default_id}" \
    | jq -rc '.data')
ingress_rules=$(echo "${security_list}" | jq -rc '.["ingress-security-rules"]')
for port in $port_list; do
    ingress_rule=$(echo '{
        "protocol":"6",
        "source":"0.0.0.0/0",
        "source-type":"CIDR_BLOCK",
        "tcp-options": {
            "destination-port-range": {
                "min": "'${port}'",
                "max": "'${port}'"
            }
        }
    }' | jq -rc)
ingress_rules=$(echo "${ingress_rules}" | jq -rc --arg ingress_rule "${ingress_rule}" '. + [ $ingress_rule | fromjson ]')
done
security_list=$(oci network security-list update \
    --security-list-id "${security_list_default_id}" \
    --display-name "default security list for ${display_name}" \
    --ingress-security-rules "${ingress_rules}" \
    --force \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
ingress_rules=$(echo "${security_list}" | jq -rc '.["ingress-security-rules"]')
echo "Updated Ingress Rules in Security List (default): $(echo "${security_list}" | jq -r '."display-name"')"

echo -e "\nCreating Security List (private) ..."
security_list=$(oci network security-list create \
    --compartment-id "${compartment_id}" \
    --display-name "security list for private subnet-${display_name}" \
    --egress-security-rules "[]" \
    --ingress-security-rules "[]" \
    --vcn-id "${vcn_id}" \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
security_list_private_id=$(echo "${security_list}" | jq -r '.id')
echo "Created Security List (private): $(echo "${security_list}" | jq -r '."display-name"')"

echo -e "\nUpdating Egress Rules in Security List (private) ..."
egress_rules=$(echo "${security_list}" | jq -rc '.["egress-security-rules"]')
egress_rule=$(echo '{
    "destination":"0.0.0.0/0",
    "destination-type":"CIDR_BLOCK",
    "protocol":"all"
}' | jq -rc)
egress_rules=$(echo "${egress_rules}" | jq -rc --arg egress_rule "${egress_rule}" '. + [ $egress_rule | fromjson ]')
security_list=$(oci network security-list update \
    --security-list-id "${security_list_private_id}" \
    --egress-security-rules "${egress_rules}" \
    --force \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
egress_rules=$(echo "${security_list}" | jq -rc '.["egress-security-rules"]')
echo "Updated Egress Rules in Security List (private): $(echo "${security_list}" | jq -r '."display-name"')"

echo -e "\nUpdating Ingress Rules in Security List (private) ..."
ingress_rules=$(echo "${security_list}" | jq -rc '.["ingress-security-rules"]')
ingress_rule=$(echo '{
    "protocol":"6",
    "source":"'${vcn_cidr_block}'",
    "source-type":"CIDR_BLOCK",
    "tcp-options": {
        "destination-port-range": {
            "min": 22,
            "max": 22
        }
    }
}' | jq -rc)
ingress_rules=$(echo "${ingress_rules}" | jq -rc --arg ingress_rule "${ingress_rule}" '. + [ $ingress_rule | fromjson ]')
ingress_rule=$(echo '{
    "icmp-options": {
        "code": 4,
        "type": 3
    },
    "protocol":"1",
    "source":"0.0.0.0/0",
    "source-type":"CIDR_BLOCK"
}' | jq -rc)
ingress_rules=$(echo "${ingress_rules}" | jq -rc --arg ingress_rule "${ingress_rule}" '. + [ $ingress_rule | fromjson ]')
ingress_rule=$(echo '{
    "icmp-options": {
        "type": 3
    },
    "protocol":"1",
    "source":"'${vcn_cidr_block}'",
    "source-type":"CIDR_BLOCK"
}' | jq -rc)
ingress_rules=$(echo "${ingress_rules}" | jq -rc --arg ingress_rule "${ingress_rule}" '. + [ $ingress_rule | fromjson ]')
security_list=$(oci network security-list update \
    --security-list-id "${security_list_private_id}" \
    --ingress-security-rules "${ingress_rules}" \
    --force \
    --wait-for-state "AVAILABLE" 2> /dev/null \
    | jq -rc '.data')
ingress_rules=$(echo "${security_list}" | jq -rc '.["ingress-security-rules"]')
echo "Updated Ingress Rules in Security List (private): $(echo "${security_list}" | jq -r '."display-name"')"

echo -e "\nLaunching Instance ..."
instance=$(oci compute instance launch \
    --compartment-id "${compartment_id}" \
    --display-name "${display_name}" \
    --availability-domain "${availability_domain}" \
    --subnet-id "${subnet_public_id}" \
    --assign-public-ip false \
    --image-id "${image_id}" \
    --shape "${shape}" \
    --shape-config '{
        "memory-in-gbs": 6.0,
        "ocpus": 1.0,
        "vcpus": 1
    }' \
    --boot-volume-size-in-gbs "${boot_volume_size_in_gbs}" \
    --is-pv-encryption-in-transit-enabled true \
    --skip-source-dest-check true \
    --agent-config '{
        "plugins-config": [
            {"desired-state": "DISABLED", "name": "Vulnerability Scanning"},
            {"desired-state": "DISABLED", "name": "Oracle Java Management Service"},
            {"desired-state": "DISABLED", "name": "OS Management Service Agent"},
            {"desired-state": "DISABLED", "name": "OS Management Hub Agent"},
            {"desired-state": "DISABLED", "name": "Management Agent"},
            {"desired-state": "ENABLED", "name": "Custom Logs Monitoring"},
            {"desired-state": "ENABLED", "name": "Compute Instance Run Command"},
            {"desired-state": "ENABLED", "name": "Compute Instance Monitoring"},
            {"desired-state": "ENABLED", "name": "Cloud Guard Workload Protection"},
            {"desired-state": "DISABLED", "name": "Block Volume Management"},
            {"desired-state": "DISABLED", "name": "Bastion"}
        ]
    }' \
    --availability-config '{
        "is-live-migration-preferred": true,
        "recovery-action": "RESTORE_INSTANCE"
    }' \
    --ssh-authorized-keys-file "${ssh_authorized_keys_file}" \
    --wait-for-state "RUNNING" 2> /dev/null \
    | jq -rc '.data')
instance_id=$(echo "${instance}" | jq -r '.id')
echo "Created Instance: $(echo "${instance}" | jq -r '."display-name"')"

vnic_id=$(oci compute instance list-vnics \
    --instance-id "${instance_id}" \
    --query 'data[].id' \
    --output json | jq -r '.[]')

vnic_dn=$(oci compute instance list-vnics \
    --instance-id "${instance_id}" \
    --query 'data[]."display-name"' \
    --output json | jq -r '.[]')
echo "VNIC attached to Instance: $vnic_dn"

private_ip_id=$(oci network private-ip list \
    --vnic-id "${vnic_id}" \
    --query 'data[].id' \
    --output json | jq -r '.[]')

public_ip_id=$(oci network public-ip list \
    --compartment-id "${compartment_id}" \
    --scope "REGION" \
    --all \
    --query "data[?\"display-name\"=='${ip_reserved_dn}'].id" \
    --output json | jq -r '.[]')

oci network public-ip update \
    --public-ip-id "${public_ip_id}" \
    --private-ip-id "${private_ip_id}" \
    --force \
    --wait-for-state "ASSIGNED" 2> /dev/null
echo "Public IP assigned to Instance: $ip_reserved_dn"

boot_volume_dn=$(oci bv boot-volume list \
    --compartment-id "${compartment_id}" \
    --query "data[?contains(\"display-name\", '${display_name}') && \"lifecycle-state\"=='AVAILABLE'].\"display-name\"" \
    --output json | jq -r '.[]')
echo "Boot Volume attached to Instance: $boot_volume_dn"




# false true OR "false" "true"   ???

# instance:
# --vnic-display-name [text]
# --hostname-label [text]

# https://github.com/oracle/oci-cli/blob/master/services/core/examples_and_test_scripts/launch_instance_example.sh
# https://github.com/oracle/oci-cli/tree/master/services/core/examples_and_test_scripts
# https://docs.oracle.com/en-us/iaas/tools/oci-cli/3.51.8/oci_cli_docs/cmdref/compute/instance/launch.html
# https://docs.oracle.com/en-us/iaas/tools/oci-cli/3.51.8/oci_cli_docs/index.html
# https://docs.oracle.com/en-us/iaas/tools/oci-cli/3.51.8/oci_cli_docs/
# https://docs.oracle.com/iaas/tools/oci-cli/latest

# https://docs.oracle.com/en-us/iaas/tools/oci-cli/3.51.8/oci_cli_docs/cmdref/network/security-list/update.html
# https://docs.oracle.com/en-us/iaas/Content/Network/Concepts/update-securitylist.htm


# Monthly Budget:
# oci budgets budget create \
#     --compartment-id "${compartment_id}" \
#     --display-name "MonthlyBudget" \
#     --budget-details '{"budgetType": "COST", "timePeriod": {"startDate": "2023-01-01", "endDate": "2023-12-31"}, "amount": 1000, "currency": "USD", "thresholds": [{"thresholdType": "PERCENTAGE", "threshold": 80}, {"thresholdType": "ABSOLUTE", "threshold": 1000}]} }' \
#     --is-enabled true

# Reserved Public IP:
# oci network public-ip create --compartment-id "${compartment_id}" --display-name "ReservedPublicIP" --lifetime "RESERVED"
# oci network public-ip list --compartment-id "${compartment_id}" --scope "REGION"
# oci network public-ip delete --public-ip-id $public_ip_id --force

