#!/bin/bash

mydotfiles=https://gitlab.com/ksz/dotfiles.git
mysetup=https://gitlab.com/ksz/setup.git
mypkgs=https://gitlab.com/ksz/setup/-/raw/main/fedora/pkgs-*.ini
mybloat=https://gitlab.com/ksz/setup/-/raw/main/fedora/bloat-*.ini
myrepos=https://gitlab.com/ksz/setup/-/raw/main/fedora/repos-*.sh
myuser1=$(grep 1000 /etc/passwd | cut -d \: -f 1)
mylog="$HOME/.local/src/setup.log"
export PATH="$HOME/.local/bin:$PATH"

function_select () {
    echo "- Choose machine:"
    echo "  1) phys 2) qemu 3) vbox"
    while true; do
        read -p "  Enter a number: " mymachine
        case "$mymachine" in
            1) mymachine=phys; break ;;
            2) mymachine=qemu; break ;;
            3) mymachine=vbox; break ;;
            *) echo "  Invalid input - try again."
        esac
    done
    if [ "$mymachine" = "phys" ]; then
        echo "- Choose hostname:"
        echo "  1) op7050 2) k52jc 3) other"
        while true; do
            read -p "  Enter a number: " myhost
            case "$myhost" in
                1) myhost=OP7050; break ;;
                2) myhost=K52JC; break ;;
                3) read -p "- Type in a hostname: " myhost;
                   echo "  Hostname has been changed."; break ;;
                *) echo "  Invalid input - try again."
            esac
        done
    fi
    echo "- Choose DE/WM:"
    echo "  1) lxqt 2) labwc 3) openbox"
    while true; do
        read -p "  Enter a number: " mydewm
        case "$mydewm" in
            1) mydewm=lxqt; break ;;
            2) mydewm=labwc; break ;;
            3) mydewm=openbox; break ;;
            *) echo "  Invalid input - try again."
        esac
    done
    if [ "$mydewm" = "lxqt" ]; then
        echo "- Choose users:"
        echo "  1) single 2) multi"
        while true; do
            read -p "  Enter a number: " myusers
            case "$myusers" in
                1) myusers=single; break ;;
                2) myusers=multi; break ;;
                *) echo "  Invalid input - try again."
            esac
        done
    fi
    if [ "$myusers" = "multi" ]; then
        read -p "- Type in a new user name: " myuser2
        sudo useradd -m "$myuser2"
        while true; do
            sudo passwd "$myuser2"
            if [ $? -eq 0 ]; then break; fi
        done
        sudo usermod -aG wheel "$myuser2"
    fi
    mypkgs=$(echo "$mypkgs" | sed "s/\*/$mydewm/")
    curl -sLo ~/pkgs.ini "$mypkgs"
    mybloat=$(echo "$mybloat" | sed "s/\*/$mydewm/")
    curl -sLo ~/bloat.ini "$mybloat"
    myrepos=$(echo "$myrepos" | sed "s/\*/$mydewm/")
    curl -sLo ~/repos.sh "$myrepos"
    read -p "- Do you want to edit the list of packages before installation? [y/N] " yn
    case "$yn" in
        y) nano -l ~/pkgs.ini ;;
        *) echo "  The list of packages remains unchanged." ;;
    esac
    read -p "- Do you want to edit the list of bloatware before installation? [y/N] " yn
    case "$yn" in
        y) nano -l ~/bloat.ini ;;
        *) echo "  The list of bloatware remains unchanged." ;;
    esac
    read -p "- Do you want to edit the list of repositories before installation? [y/N] " yn
    case "$yn" in
        y) nano -l ~/repos.sh ;;
        *) echo "  The list of repositories remains unchanged." ;;
    esac
    mkdir -p ~/.local/src && echo -e "$(date)\n" > "$mylog"
    echo
    function_install 2>&1 | tee -a "$mylog"
}

function_install () {
    echo "%wheel ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/01-no-password
    echo "Defaults timestamp_timeout=15" | sudo tee /etc/sudoers.d/02-timeout
    if [ "$mymachine" != "phys" ]; then myhost=VIRT; fi
    sudo hostnamectl set-hostname $myhost
    sudo cp /etc/dnf/dnf.conf /etc/dnf/dnf.conf.bak
    sudo tee -a /etc/dnf/dnf.conf <<EOF
max_parallel_downloads=10
fastestmirror=True
defaultyes=True
install_weak_deps=False
EOF
    sudo dnf config-manager setopt installonly_limit=2
    if [ "$mymachine" != "phys" ]; then sed -i "/open-vm/d;/spice/d;/virtualbox/d;/qxl/d;/vmware/d" ~/bloat.ini; fi
    grep -E -v '^; |^$' ~/bloat.ini | xargs sudo dnf -y remove
    sudo dnf -y autoremove
    bash ~/repos.sh
    sudo dnf -y upgrade --refresh
    # sudo dnf -y group install "$myde desktop" --exclude=falkon --exclude=dnfdragora-updater --exclude=gnome-disk-utility
    sudo dnf -y swap ffmpeg-free ffmpeg --allowerasing
    if [ "$mymachine" = "vbox" ]; then sudo usermod -aG vboxsf "$myuser1"; fi
    grep -E -v '^; |^D |^$' ~/pkgs.ini | xargs sudo dnf -y install
    if [ $? -ne 0 ]; then grep -E -v '^; |^D |^$' ~/pkgs.ini | xargs -n 1 sudo dnf -y install; fi
    grep -E '^D ' ~/pkgs.ini | cut -c 3- | while read line; do sh -c "$line"; done
    # sudo systemctl set-default graphical.target
    sudo systemctl enable fstrim.timer
    sudo systemctl start fstrim.timer
    sudo cp /etc/default/grub /etc/default/grub.bak
    sudo sed -i "/GRUB_TIMEOUT=/s/5/2/" /etc/default/grub
    sudo grub2-mkconfig -o /boot/grub2/grub.cfg
    sudo tee /etc/sddm.conf.d/sddm.conf <<EOF
[Autologin]
#User=
#Session=$mydewm.desktop
[General]
InputMethod=
[Theme]
#Current=catppuccin-frappe
#ThemeDir=/usr/share/sddm/themes
[X11]
#DisplayCommand=/etc/sddm/Xsetup
#DisplayStopCommand=/etc/sddm/Xstop
EOF
    if [ "$mydewm" != "lxqt" ]; then sudo sed -i "/^#Current=/s/^#//;/^#ThemeDir=/s/^#//" /etc/sddm.conf.d/sddm.conf; fi
    git clone --depth 1 "$mydotfiles" ~/.local/src/dotfiles
    git clone --depth 1 "$mysetup" ~/.local/src/setup
    find ~/.local/src/dotfiles -type f ! -name "setup-*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser1/" "{}" \;
    find ~/.local/src/setup -type f ! -name "setup*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser1/" "{}" \;
    rsync -r ~/.local/src/dotfiles/$mydewm/ ~/.
    bash post-install.sh fedora fedora
    if [ "$myusers" = "multi" ]; then
        sudo sed -i "/^#User=/s/^.*$/User=$myuser2/;/^#Session=/s/^#//" /etc/sddm.conf.d/sddm.conf
        sudo -u "$myuser2" bash <<EOF
cd \$HOME && mkdir -p \$HOME/.local/src
git clone --depth 1 $mydotfiles \$HOME/.local/src/dotfiles
git clone --depth 1 $mysetup \$HOME/.local/src/setup
find \$HOME/.local/src/dotfiles -type f ! -name "setup-*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser2/" "{}" \;
find \$HOME/.local/src/setup -type f ! -name "setup*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser2/" "{}" \;
rsync -r \$HOME/.local/src/dotfiles/$mydewm/ \$HOME/.
bash \$HOME/.local/bin/post-install.sh fedora fedora
EOF
    fi
    cd $HOME
    if [ "$mydewm" != "lxqt" ]; then sudo btrfs filesystem label / ROOT; fi
    echo "%wheel ALL=(ALL) NOPASSWD: /bin/mount,/bin/umount,/sbin/grub2-reboot" | sudo tee /etc/sudoers.d/01-no-password
    rm -f ~/bloat.ini ~/pkgs.ini ~/repos.sh ~/setup.sh
    reboot
}

case $1 in
    -i) function_install ;;
    * ) function_select ;;
esac
