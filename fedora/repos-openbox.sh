#!/bin/bash

sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf -y copr enable zawertun/hack-fonts
sudo dnf -y copr enable mamg22/nsxiv
# sudo dnf -y copr enable vishalvvr/tint2
sudo dnf -y install https://dl.fedoraproject.org/pub/epel/9/Everything/x86_64/Packages/t/tint2-17.0.2-4.el9.x86_64.rpm
