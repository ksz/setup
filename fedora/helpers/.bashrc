# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]; then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
    for rc in ~/.bashrc.d/*; do
        if [ -f "$rc" ]; then
            . "$rc"
        fi
    done
fi
unset rc

alias ping='ping -c 5'
alias sudo='sudo '
alias wget='wget -c'
alias bc='bc -lq'
alias df='df -h'
alias du='du -ah'
alias ht='htop -t'
alias ka='killall'
alias ll='ls -la'
alias mc='mc -x'
alias nn='nano -l'
alias qr='qrcode-show.sh'
alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -Iv'
alias md='mkdir -pv'
alias rd='rmdir -v'
# alias xp='xprop | grep "WM_WINDOW_ROLE\|WM_CLASS" && echo "WM_CLASS(STRING) = \"NAME\", \"CLASS\""'
alias xp='obxprop | grep "^_OB_APP"'
alias or='openbox --reconfigure'
alias ud='update-desktop-database $HOME/.local/share/applications'
# alias sdu='sudo dnf upgrade --refresh'
alias sdu='sudo dnf upgrade'
alias sda='sudo dnf autoremove'
alias sdi='sudo dnf install'
alias sdr='sudo dnf remove'
alias sdl='dnf list --installed | grep'
alias gil='git-pu.sh -l'
alias gis='git-pu.sh -s'

PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
