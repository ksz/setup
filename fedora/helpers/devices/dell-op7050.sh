#!/bin/bash

myhelp="$HOME/.local/src/setup/fedora/helpers"

apps-install.sh --eget
apps-install.sh --rustdesk
sudo cp $myhelp/{00-keyboard,10-intel}.conf /etc/X11/xorg.conf.d
sudo cp $myhelp/00-mount-internal.rules /etc/polkit-1/rules.d
sudo sed -i "/^#DisplayCommand=/s/^#//" /etc/sddm.conf.d/sddm.conf
echo "xrandr --output DP1 --auto --output DP2 --off --output HDMI1 --off" | sudo tee -a /etc/sddm/Xsetup
echo "xdotool mousemove_relative 400 200" | sudo tee -a /etc/sddm/Xsetup
sed -i "/Hidden=/s/false/true/" ~/.config/autostart/lxqt-config-monitor-autostart.desktop

sudo systemctl disable avahi-daemon.socket
sudo systemctl disable avahi-daemon.service
sudo systemctl disable cups.socket
sudo systemctl disable cups.path
