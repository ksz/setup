#!/bin/bash

myuser1=$(grep 1000 /etc/passwd | cut -d \: -f 1)
myuser2=$(grep 1001 /etc/passwd | cut -d \: -f 1)
myhelp="$HOME/.local/src/setup/fedora/helpers"

if whoami | grep -q "$myuser1"; then
    :
else
    echo "Log in as \"$myuser1\" and run the script again."
    exit
fi

apps-install.sh --eget
apps-install.sh --anydesk
apps-install.sh --rustdesk
sudo cp $myhelp/{00-keyboard,10-intel,40-libinput}.conf /etc/X11/xorg.conf.d
sudo sed -i "s/pc105/asus_laptop/" /etc/X11/xorg.conf.d/00-keyboard.conf
sudo cp $myhelp/00-mount-internal.rules /etc/polkit-1/rules.d
sudo sed -i "/^#DisplayCommand=/s/^#//" /etc/sddm.conf.d/sddm.conf
echo "xrandr --output LVDS-1 --auto --output VGA-1-1 --off" | sudo tee -a /etc/sddm/Xsetup
# echo "xdotool mousemove_relative 400 200" | sudo tee -a /etc/sddm/Xsetup
sed -i "/Hidden=/s/false/true/" ~/.config/autostart/lxqt-config-monitor-autostart.desktop
# sed -i "/Watcher=false$/s/false/true/g" ~/.config/lxqt/lxqt-powermanagement.conf
nmcli radio wifi off
sudo -u "$myuser2" bash <<EOF
sed -i "/Hidden=/s/false/true/" \$HOME/.config/autostart/lxqt-config-monitor-autostart.desktop
# sed -i "/Watcher=false$/s/false/true/g" \$HOME/.config/lxqt/lxqt-powermanagement.conf
nmcli radio wifi off
cp -f /etc/xdg/autostart/anydesk*.desktop \$HOME/.config/autostart
for i in \$(ls \$HOME/.config/autostart/anydesk*.desktop); do echo "Hidden=true" >> \$i; done
EOF
sudo dnf -y remove xorg-x11-drv-nouveau
sudo cp $myhelp/00-nvidia-remove.rules /etc/udev/rules.d
echo "blacklist nouveau" | sudo tee /etc/modprobe.d/nouveau-blacklist.conf
echo "options nouveau modeset=0" | sudo tee -a /etc/modprobe.d/nouveau-blacklist.conf
sudo sed -i '/^GRUB_CMDLINE_LINUX=/s/"$/ rd.driver.blacklist=nouveau"/' /etc/default/grub
# echo "options snd_hda_intel enable=1,0" | sudo tee /etc/modprobe.d/snd-hda-intel.conf
sudo grub2-mkconfig -o /boot/grub2/grub.cfg
sudo dracut --force /boot/initramfs-$(uname -r).img $(uname -r)

sudo systemctl disable avahi-daemon.socket
sudo systemctl disable avahi-daemon.service
sudo systemctl disable cups.socket
sudo systemctl disable cups.path



# mkdir /tmp/nouveau
# cd /tmp/nouveau
# wget https://raw.github.com/envytools/firmware/master/extract_firmware.py
# wget https://download.nvidia.com/XFree86/Linux-x86_64/340.32/NVIDIA-Linux-x86_64-340.32.run
# sh NVIDIA-Linux-x86_64-340.32.run --extract-only
# python extract_firmware.py
# sudo mkdir /lib/firmware/nouveau
# sudo cp -d nv* vuc-* /lib/firmware/nouveau

# https://nouveau.freedesktop.org/VideoAcceleration.html#firmware
# https://github.com/rpmfusion/nouveau-firmware

# find /lib/modules/$(uname -r) -type f -name '*asus*'
# lsmod | grep asus
# sudo dmesg --level=err,warn
# sudo dmesg --level=emerg,alert,crit,err


### Fedora KDE Plasma Desktop Spin
# sudo dnf remove \*akonadi\*
