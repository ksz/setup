/run/media/ksz/Ventoy/
├── iso
│   ├── install
│   │   ├── archlinux-2024.05.01-x86_64.iso
│   │   ├── artix-base-runit-20230814-x86_64.iso
│   │   ├── debian-12.5.0-amd64-netinst.iso
│   │   ├── devuan_daedalus_5.0.1_amd64_netinstall.iso
│   │   └── Fedora-LXQt-Live-x86_64-40-1.14.iso
│   └── tools
│       ├── clonezilla-live-3.1.2-22-amd64.iso
│       ├── gparted-live-1.6.0-3-amd64.iso
│       ├── kaisenlinuxrolling2.3-amd64-SR.iso
│       ├── kali-linux-2024.1-live-amd64.iso
│       ├── kodachi-8.27-64-kernel-6.2.iso
│       ├── systemrescue-11.01-amd64.iso
│       ├── systemrescuecd-x86-5.1.1.iso
│       └── tails-amd64-6.3.iso
├── storage
│   └── .ventoyignore
└── ventoy
    └── ventoy.json

6 directories, 15 files
