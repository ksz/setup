#!/bin/sh

mydotfiles=https://gitlab.com/ksz/dotfiles.git
mysetup=https://gitlab.com/ksz/setup.git
mypkgs=https://gitlab.com/ksz/setup/-/raw/main/debian/pkgs-*.ini
myuser1=$(grep 1000 /etc/passwd | cut -d \: -f 1)
myuser2=$(grep 1001 /etc/passwd | cut -d \: -f 1)
mylog1="/home/$myuser1/.local/src/setup.log"
mylog2="/home/$myuser2/.local/src/setup.log"
export PATH="$HOME/.local/bin:$PATH"

mymachine=NA
myrelease=NA
myde=NA
myusers=NA

function_select () {
    echo "- Choose machine:"
    echo "  1) phys 2) qemu 3) vbox"
    while true; do
        read -p "  Enter a number: " mymachine
        case "$mymachine" in
            1) mymachine=phys; break ;;
            2) mymachine=qemu; break ;;
            3) mymachine=vbox; break ;;
            *) echo "  Invalid input - try again."
        esac
    done
    echo "- Choose release:"
    echo "  1) stable 2) testing 3) sid"
    while true; do
        read -p "  Enter a number: " myrelease
        case "$myrelease" in
            1) myrelease=stable; break ;;
            2) myrelease=testing; break ;;
            3) myrelease=sid; break ;;
            *) echo "  Invalid input - try again."
        esac
    done
    echo "- Choose DE:"
    echo "  1) lxqt 2) mate 3) xfce"
    while true; do
        read -p "  Enter a number: " myde
        case "$myde" in
            1) myde=lxqt; break ;;
            2) myde=mate; break ;;
            3) myde=xfce; break ;;
            *) echo "  Invalid input - try again."
        esac
    done
    echo "- Choose users:"
    echo "  1) single 2) multi"
    while true; do
        read -p "  Enter a number: " myusers
        case "$myusers" in
            1) myusers=single; break ;;
            2) myusers=multi; break ;;
            *) echo "  Invalid input - try again."
        esac
    done
    sed -i "/^mymachine=NA$/s/NA/$mymachine/;/^myrelease=NA$/s/NA/$myrelease/" ~/setup.sh
    sed -i "/^myde=NA$/s/NA/$myde/;/^myusers=NA$/s/NA/$myusers/" ~/setup.sh
    mypkgs=$(echo "$mypkgs" | sed "s/\*/$myde/")
    wget -qO ~/pkgs.ini "$mypkgs"
    echo "- Set a password for the root account:"
    while true; do
        sudo passwd root
        if [ $? -eq 0 ]; then break; fi
    done
    if [ "$myusers" = "multi" ]; then
        read -p "- Type in a new user name: " myuser2
        sudo adduser --gecos "$myuser2" "$myuser2"
        sudo usermod -aG cdrom,floppy,sudo,audio,dip,video,plugdev,netdev "$myuser2"
    fi
    read -p "- Do you want to edit the list of packages before installation? [y/N] " yn
    case "$yn" in
        y) nano -l ~/pkgs.ini ;;
        *) echo "  The list of packages remains unchanged." ;;
    esac
    if [ "$mymachine" = "vbox" ] && [ "$myrelease" != "sid" ]; then
        read -p "- Insert Guest Additions CD image and press any key to continue ..." continue
    fi
    mkdir -p ~/.local/src && echo "$(date)\n" > "$mylog1"
    echo
    function_install1 2>&1 | tee -a "$mylog1"
}

function_install1 () {
    echo "$myuser1 ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/$myuser1
    echo "Defaults timestamp_timeout=15" | sudo tee /etc/sudoers.d/timeout
    sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak
    if [ "$myrelease" = "stable" ]; then
        printf "\n" | sudo tee -a /etc/apt/sources.list
        . /etc/os-release
        echo "deb http://deb.debian.org/debian $VERSION_CODENAME-backports main non-free-firmware" | sudo tee -a /etc/apt/sources.list
        echo "deb-src http://deb.debian.org/debian $VERSION_CODENAME-backports main non-free-firmware" | sudo tee -a /etc/apt/sources.list
        sudo sed -i "s/main non-free-firmware$/main non-free-firmware contrib non-free/" /etc/apt/sources.list
    fi
    if [ "$myrelease" = "testing" ]; then
        . /etc/os-release
        sudo sed -i "s/$VERSION_CODENAME/testing/" /etc/apt/sources.list
        sudo sed -i "s/main non-free-firmware$/main non-free-firmware contrib non-free/" /etc/apt/sources.list
    fi
    if [ "$myrelease" = "sid" ]; then
        echo "deb http://deb.debian.org/debian/ sid main non-free-firmware contrib non-free" | sudo tee /etc/apt/sources.list
        echo "deb-src http://deb.debian.org/debian/ sid main non-free-firmware contrib non-free" | sudo tee -a /etc/apt/sources.list
    fi
    sudo apt-get update
    if sudo apt-get --dry-run install linux-image-amd64 | grep "is already the newest version"; then
        function_install2 2>&1 | tee -a "$mylog1"
    else
        sudo apt-get -y install linux-image-amd64
        sudo mkdir /etc/systemd/system/getty@.service.d
        printf "[Service]\nExecStart=\nExecStart=-/sbin/agetty --noclear --autologin $(whoami) %%I \$TERM\n" | \
        sudo tee /etc/systemd/system/getty@.service.d/autologin.conf
        echo "@reboot sleep 5 && sh /home/$myuser1/setup.sh -i2 | tee -a /dev/tty1 2>&1 >> $mylog1" | tee ~/cronjobs.txt
        crontab ~/cronjobs.txt
        sudo shutdown -r now
    fi
}

function_install2 () {
    if test -d /etc/systemd/system/getty@.service.d; then sudo rm -rf /etc/systemd/system/getty@.service.d; fi
    export DEBIAN_FRONTEND=noninteractive
    echo "* libraries/restart-without-asking boolean true" | sudo debconf-set-selections
    sudo apt-get clean
    sudo apt-get update
    if [ "$myrelease" != "stable" ]; then
        sudo apt-get -yq dist-upgrade
    else
        sudo apt-get -y upgrade
    fi
    grep -E -v '^; |^D |^$' ~/pkgs.ini | xargs sudo apt-get -y install
    if [ $? -ne 0 ]; then
        grep -E -v '^; |^D |^$' ~/pkgs.ini | xargs -n 1 sudo apt-get -y install
    fi
    grep -E '^D ' ~/pkgs.ini | cut -c 3- | while read line; do sh -c "$line"; done
    sudo apt-get -yf install
    sudo apt-get -y autoremove
    if [ "$mymachine" = "phys" ]; then
        sudo apt-get -y install firmware-linux
    fi
    if [ "$mymachine" = "phys" ] || [ "$mymachine" = "qemu" ]; then
        sudo systemctl enable fstrim.timer
        sudo systemctl start fstrim.timer
    fi
    if [ "$mymachine" = "qemu" ]; then
        sudo apt-get -y install dropbear spice-vdagent
    fi
    if [ "$mymachine" = "vbox" ]; then
        if [ "$myrelease" = "stable" ] || [ "$myrelease" = "testing" ]; then
            sudo apt-get -y install build-essential dkms linux-headers-$(uname -r)
            sudo mount /dev/sr0 /media/cdrom0
            sudo sh /media/cdrom0/VBoxLinuxAdditions.run --nox11
            sudo umount /media/cdrom0 && sudo eject /dev/sr0
        fi
        if [ "$myrelease" = "sid" ]; then sudo apt-get -y install virtualbox-guest-x11; fi
        if [ "$myusers" = "single" ]; then
            sudo usermod -aG vboxsf "$myuser1"
        else
            for i in "$myuser1" "$myuser2"; do sudo usermod -aG vboxsf $i; done
        fi
    fi
    sudo cp /etc/network/interfaces /etc/network/interfaces.bak
    sudo sed -i '/# The loopback network interface/,$d' /etc/network/interfaces
    sudo systemctl restart NetworkManager
    sudo cp /etc/default/grub /etc/default/grub.bak
    sudo sed -i "/GRUB_TIMEOUT=/s/5/2/;/^GRUB_THEME=/s/^/#/" /etc/default/grub
    sudo update-grub
    if [ "$myde" = "lxqt" ]; then
        function_nologin 2>&1
    else
        sudo mkdir /etc/lightdm/lightdm.conf.d
        sudo cp /etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf.d
        sudo sed -i "/^#greeter-hide-users=false/s/^#//" /etc/lightdm/lightdm.conf.d/lightdm.conf
        sudo sed -i "s/^#autologin-user=/autologin-user=$myuser1/;/^#autologin-user-timeout=0/s/^#//" /etc/lightdm/lightdm.conf.d/lightdm.conf
        sudo cp /etc/lightdm/lightdm-gtk-greeter.conf /etc/lightdm/lightdm-gtk-greeter.conf.bak
        sudo tee /etc/lightdm/lightdm-gtk-greeter.conf <<EOF
[greeter]
theme-name = Arc-Dark
icon-theme-name = Papirus-Dark
font-name = Cantarell 9
indicators = ~host;~spacer;~session;~clock;~power
EOF
        echo "@reboot sleep 10 && sh /home/$myuser1/setup.sh -u1 2>&1 | tee -a $mylog1" | tee ~/cronjobs.txt
        crontab ~/cronjobs.txt
        sudo shutdown -r now
    fi
}

function_nologin () {
    sudo mkdir /etc/sddm.conf.d
    sudo tee /etc/sddm.conf.d/sddm.conf <<EOF
[Autologin]
#User=
#Session=lxqt.desktop
[General]
InputMethod=
[Theme]
Current=catppuccin-frappe
ThemeDir=/usr/share/sddm/themes
[X11]
#DisplayCommand=/etc/sddm/Xsetup
#DisplayStopCommand=/etc/sddm/Xstop
EOF
    git clone --depth 1 "$mydotfiles" ~/.local/src/dotfiles
    git clone --depth 1 "$mysetup" ~/.local/src/setup
    find ~/.local/src/dotfiles -type f ! -name "setup-*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser1/" "{}" \;
    find ~/.local/src/setup -type f ! -name "setup*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser1/" "{}" \;
    rsync -r ~/.local/src/dotfiles/$myde/ ~/.
    bash post-install.sh debian debian
    if [ "$myusers" = "multi" ]; then
        sudo sed -i "/^#User=/s/^.*$/User=$myuser2/;/^#Session=/s/^#//" /etc/sddm.conf.d/sddm.conf
        sudo -u "$myuser2" bash <<EOF
cd \$HOME && mkdir -p \$HOME/.local/src
git clone --depth 1 $mydotfiles \$HOME/.local/src/dotfiles
git clone --depth 1 $mysetup \$HOME/.local/src/setup
find \$HOME/.local/src/dotfiles -type f ! -name "setup-*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser2/" "{}" \;
find \$HOME/.local/src/setup -type f ! -name "setup*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser2/" "{}" \;
rsync -r \$HOME/.local/src/dotfiles/lxqt/ \$HOME/.
bash \$HOME/.local/bin/post-install.sh debian debian
EOF
        echo "$myuser2 ALL=(ALL) NOPASSWD: /sbin/grub-reboot,/bin/mount,/bin/umount" | sudo tee /etc/sudoers.d/$myuser2
    fi
    cd $HOME
    echo "$myuser1 ALL=(ALL) NOPASSWD: /sbin/grub-reboot,/sbin/shutdown,/bin/mount,/bin/umount" | sudo tee /etc/sudoers.d/$myuser1
    rm -f ~/cronjobs.txt ~/pkgs.ini ~/setup.sh
    sudo shutdown -r now
}

function_user1 () {
    export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$(id -u "$myuser1")/bus
    if [ "$myde" = "mate" ]; then export DISPLAY=:0; elif [ "$myde" = "xfce" ]; then export DISPLAY=:0.0; fi
    git clone --depth 1 "$mydotfiles" ~/.local/src/dotfiles
    git clone --depth 1 "$mysetup" ~/.local/src/setup
    find ~/.local/src/dotfiles -type f ! -name "setup-*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser1/" "{}" \;
    find ~/.local/src/setup -type f ! -name "setup*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser1/" "{}" \;
    rsync -r ~/.local/src/dotfiles/$myde/ ~/.
    crontab -r
    bash post-install.sh debian debian
    if [ "$myusers" = "multi" ]; then
        sudo sed -i "s/$myuser1/$myuser2/" /etc/lightdm/lightdm.conf.d/lightdm.conf
        echo "@reboot sleep 10 && sh /home/$myuser2/setup.sh -u2 2>&1 | tee -a $mylog2" | sudo -u "$myuser2" tee /home/$myuser2/cronjobs.txt
        sudo crontab -u "$myuser2" /home/$myuser2/cronjobs.txt
        sudo -u "$myuser2" mkdir -p /home/$myuser2/.local/src
        sudo sh -c "cat /home/$myuser1/pkgs.ini > /home/$myuser2/pkgs.ini"
        sudo sh -c "cat /home/$myuser1/setup.sh > /home/$myuser2/setup.sh"
        sudo chown "$myuser2:$myuser2" /home/$myuser2/pkgs.ini /home/$myuser2/setup.sh
        echo "$myuser2 ALL=(ALL) NOPASSWD: /sbin/grub-reboot,/bin/mount,/bin/umount" | sudo tee /etc/sudoers.d/$myuser2
        echo "$myuser1 ALL=(ALL) NOPASSWD: /sbin/grub-reboot,/sbin/shutdown,/bin/mount,/bin/umount" | sudo tee /etc/sudoers.d/$myuser1
        rm -f ~/cronjobs.txt ~/pkgs.ini ~/setup.sh
        sudo shutdown -r now
    else
        sudo cp -f /etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf.d/lightdm.conf
        sudo sed -i "/^#greeter-hide-users=false/s/^#//" /etc/lightdm/lightdm.conf.d/lightdm.conf
        echo "$myuser1 ALL=(ALL) NOPASSWD: /sbin/grub-reboot,/sbin/shutdown,/bin/mount,/bin/umount" | sudo tee /etc/sudoers.d/$myuser1
        notify-send -t 3600000 -u normal "setup.sh" "Run scripts:\n- firefox-setup.sh\n- [firewall-setup.sh]\n- [home-encrypt.sh]"
        notify-send -t 3600000 -u normal "setup.sh" "Operation successfully completed."
        rm -f ~/cronjobs.txt ~/pkgs.ini ~/setup.sh
        exit
    fi
}

function_user2 () {
    export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$(id -u "$myuser2")/bus
    if [ "$myde" = "mate" ]; then export DISPLAY=:0; elif [ "$myde" = "xfce" ]; then export DISPLAY=:0.0; fi
    git clone --depth 1 "$mydotfiles" ~/.local/src/dotfiles
    git clone --depth 1 "$mysetup" ~/.local/src/setup
    find ~/.local/src/dotfiles -type f ! -name "setup-*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser2/" "{}" \;
    find ~/.local/src/setup -type f ! -name "setup*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser2/" "{}" \;
    rsync -r ~/.local/src/dotfiles/$myde/ ~/.
    crontab -r
    bash post-install.sh debian debian
    notify-send -t 3600000 -u normal "setup.sh" "Run scripts:\n- firefox-setup.sh\n- [firewall-setup.sh]\n- [home-encrypt.sh]"
    notify-send -t 3600000 -u normal "setup.sh" "Operation successfully completed."
    rm -f ~/cronjobs.txt ~/pkgs.ini ~/setup.sh
    exit
}

case $1 in
    -i2) function_install2 ;;
    -u1) function_user1 ;;
    -u2) function_user2 ;;
    *  ) function_select ;;
esac
