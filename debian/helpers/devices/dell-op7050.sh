#!/bin/bash

myhelp="$HOME/.local/src/setup/debian/helpers"

# sudo cp /etc/default/keyboard /etc/default/keyboard.bak
# sudo sed -i '/^XKBLAYOUT=/s/".*"/"pl,us"/' /etc/default/keyboard
# sudo sed -i '/^XKBOPTIONS=""/s/""/"caps:swapescape"/' /etc/default/keyboard
sudo cp $myhelp/00-keyboard.conf /etc/X11/xorg.conf.d

sudo apt-get -y purge xserver-xorg-video-intel
sudo cp $myhelp/10-intel.conf /etc/X11/xorg.conf.d
sudo sed -i 's/"intel"/"modesetting"/' /etc/X11/xorg.conf.d/10-intel.conf
sudo sed -i "/TearFree/d" /etc/X11/xorg.conf.d/10-intel.conf

sudo cp $myhelp/50-udisks.pkla /etc/polkit-1/localauthority/50-local.d
