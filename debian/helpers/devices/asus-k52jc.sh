#!/bin/bash

myhelp="$HOME/.local/src/setup/debian/helpers"

# sudo apt-get -y install bumblebee primus
# sudo apt-get -y install tlp

# sudo cp /etc/default/keyboard /etc/default/keyboard.bak
# sudo sed -i '/^XKBLAYOUT=/s/".*"/"pl,us"/' /etc/default/keyboard
# sudo sed -i '/^XKBOPTIONS=""/s/""/"caps:swapescape"/' /etc/default/keyboard
sudo cp $myhelp/00-keyboard.conf /etc/X11/xorg.conf.d
sudo sed -i "s/pc105/asus_laptop/" /etc/X11/xorg.conf.d/00-keyboard.conf

sudo apt-get -y purge xserver-xorg-video-intel
sudo cp $myhelp/10-intel.conf /etc/X11/xorg.conf.d
sudo sed -i 's/"intel"/"modesetting"/' /etc/X11/xorg.conf.d/10-intel.conf
sudo sed -i "/TearFree/d" /etc/X11/xorg.conf.d/10-intel.conf

sudo cp $myhelp/50-udisks.pkla /etc/polkit-1/localauthority/50-local.d

sudo cp $myhelp/70-wifi-wired-exclusive.sh /etc/NetworkManager/dispatcher.d
sudo chmod +x /etc/NetworkManager/dispatcher.d/70-wifi-wired-exclusive.sh

sudo cp $myhelp/00-nvidia-remove.rules /etc/udev/rules.d

echo "options ath9k nohwcrypt=1 ps_enable=1" | sudo tee /etc/modprobe.d/ath9k.conf
# echo "options snd_hda_intel enable=1,0" | sudo tee /etc/modprobe.d/snd-hda-intel.conf
echo "blacklist nouveau" | sudo tee /etc/modprobe.d/nouveau-blacklist.conf
echo "options nouveau modeset=0" | sudo tee -a /etc/modprobe.d/nouveau-blacklist.conf
sudo update-initramfs -u


# sudo dd if=/dev/zero of=/swapfile bs=1M count=3k status=progress
# sudo chmod 600 /swapfile && sudo mkswap /swapfile && sudo swapon /swapfile
# echo "/swapfile none swap sw 0 0" | sudo tee -a /etc/fstab
# echo "vm.swappiness=10" | sudo tee -a /etc/sysctl.d/local.conf

# find /lib/modules/$(uname -r) -type f -name '*asus*'
# lsmod | grep asus
# sudo dmesg --level=err,warn
# sudo dmesg --level=emerg,alert,crit,err
