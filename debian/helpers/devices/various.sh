#!/bin/bash

myhelp="$HOME/.local/src/setup/debian/helpers"
# sudo cp /etc/default/keyboard /etc/default/keyboard.bak
# sudo sed -i '/^XKBOPTIONS=""/s/""/"caps:swapescape"/' /etc/default/keyboard
sudo cp $myhelp/50-udisks.pkla /etc/polkit-1/localauthority/50-local.d

# Dell Inspiron 3558 ----------------------------------------------------------
sudo apt install firmware-iwlwifi

# Lenovo G50-30 ---------------------------------------------------------------
sudo apt install firmware-realtek
# Samsung ML-2165W
sudo apt install cups system-config-printer libcupsimage2
sudo usermod -aG lpadmin wiesiek
wget https://www.bchemnet.com/suldr/driver/UnifiedLinuxDriver-1.00.36.tar.gz
tar xzf U*L*D*.tar.gz
cd uld && sudo ./install-printer.sh
rm -rf uld U*L*D*.tar.gz

# Dell Latitude E6500 ---------------------------------------------------------
sudo apt install broadcom-sta-dkms     # Lenovo pvl/wsk ???
sudo sed -i '/GRUB_CMDLINE_LINUX=/s/""/"nouveau.noaccel=1"/' /etc/default/grub
sudo update-grub
xfconf-query -c xsettings -n -t int -p "/Xft/DPI" -s 124
sudo cp $myhelp/40-libinput.conf /etc/X11/xorg.conf.d
# Epson L3060
sudo apt install cups system-config-printer libcupsimage2
sudo apt install xsane sane-airscan printer-driver-escpr
sudo usermod -aG lpadmin mario

# HP 530 (debian-11.3.0-i386-netinst.iso) -------------------------------------
sudo apt install firmware-iwlwifi
sudo modprobe -r iwl3945 && sudo modprobe iwl3945
sudo cp $myhelp/40-libinput.conf /etc/X11/xorg.conf.d
