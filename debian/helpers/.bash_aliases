alias \
    ..='cd ..' \
    ...='cd ../../' \
    ....='cd ../../../' \
    ip='ip -color=auto' \
    ls='ls --color=auto' \
    diff='diff --color=auto' \
    grep='grep --color=auto' \
    egrep='egrep --color=auto' \
    fgrep='fgrep --color=auto' \
    ping='ping -c 5' \
    sudo='sudo ' \
    wget='wget -c' \
    bc='bc -lq' \
    df='df -h' \
    du='du -ah' \
    ht='htop -t' \
    ka='killall' \
    ll='ls -la' \
    mc='mc -x' \
    nn='nano -l' \
    cp='cp -iv' \
    mv='mv -iv' \
    rm='rm -Iv' \
    md='mkdir -pv' \
    rd='rmdir -v' \
    xp='xprop | grep "WM_WINDOW_ROLE\|WM_CLASS" && echo "WM_CLASS(STRING) = \"NAME\", \"CLASS\""' \
    or='openbox --reconfigure' \
    ud='update-desktop-database $HOME/.local/share/applications' \
    sau='sudo apt update && sudo apt upgrade' \
    saa='sudo apt autoremove' \
    sai='sudo apt install' \
    sar='sudo apt remove' \
    sap='sudo apt purge' \
    sab='. /etc/os-release && sudo apt install -t $VERSION_CODENAME-backports' \
    sal='apt list --installed | grep'
