#!/bin/bash

mydotfiles=https://gitlab.com/ksz/dotfiles.git
mysetup=https://gitlab.com/ksz/setup.git
mypkgs=https://gitlab.com/ksz/setup/-/raw/main/termux/pkgs.ini

termux-setup-storage

curl -sLO "$mypkgs"
read -p "- Do you want to edit the list of packages before installation? [y/N] " yn
case "$yn" in
    y) nano -l ~/pkgs.ini ;;
    *) echo "  The list of packages remains unchanged." ;;
esac
echo

pkg upgrade -y
grep -E -v '^; |^G |^P |^$' ~/pkgs.ini | xargs pkg install -y
if [ $? -ne 0 ]; then
    grep -E -v '^; |^G |^P |^$' ~/pkgs.ini | xargs apt install -y
fi
grep -E '^P ' ~/pkgs.ini | cut -c 3- | while read line; do sh -c "$line"; done
grep -E '^G ' ~/pkgs.ini | cut -c 3- | while read line; do sh -c "$line"; done

mkdir -p ~/.local/src
git clone --depth 1 "$mydotfiles" ~/.local/src/dotfiles
git clone --depth 1 "$mysetup" ~/.local/src/setup
rsync -r ~/.local/src/dotfiles/termux/ ~/.
rm -f ~/pkgs.ini ~/setup.sh

sed -i "s/PasswordAuthentication yes/PasswordAuthentication no/" $PREFIX/etc/ssh/sshd_config

rsync -r /data/data/com.termux/files/home/storage/shared/.home/ /data/data/com.termux/files/home/.
chmod +x ~/bin/*.sh
chmod 700 ~/.ssh
chmod 600 ~/.ssh/*
