# Arch / Artix

`curl -sL ksz.gitlab.io/ar | sh`

`sh setup.sh`

- _archlinux-***-x86_64.iso_ || _artix-base-runit-***-x86_64.iso_

**Physical machine:**
- `/dev/nvme0n1p1` (fat32, boot) >= 1 GB
- `/dev/nvme0n1p2` (ext4) >= 100 GB
- `/dev/nvme0n1p3` (ext4) = rest of the disk

**Virtual machines:**
- `/dev/sda` & `/dev/vda` >= 20 GB

**Artix:**
- _From CD/DVD/ISO: artix.x86_64_
- `root` : `artix` || `artix` : `artix` ---> `sudo su`

# Debian / Devuan

`wget -qO - ksz.gitlab.io/de | sh`

`curl -sL ksz.gitlab.io/de | sh`

- _debian-***-amd64-netinst.iso_ || _devuan__***__amd64_netinstall.iso_

**Manual install:**
- _Graphical install / Install_
- root account disabled (empty password)
- `$myuser1` account created
- installed only _standard system utilities_
- _sysvinit_ (Devuan)

**Automated install:**
- _Advanced options > Automated install / Automated install_
- `ksz.gitlab.io/de*.cfg`

# Fedora

`curl -sL ksz.gitlab.io/fe | sh`

- _Fedora-LXQt-Live-x86_64-***.iso_

**Installation summary:**
- Keyboard: _Polish, English (US)_
- Time & Date: _Europe/Warsaw timezone_
- Installation Destination: _Automatic partitioning selected_
- Network & Host Name: _Connected_ `enp0s3`
- Root Account: _Root password is set_
- User Creation: _Administrator_ `$myuser` _will be created_

`Ctrl+Alt+F3`

# Oracle

`curl -sL ksz.gitlab.io/or | sh`

- _OracleLinux-*-x86_64-boot.iso_

**Installation summary:**
- Keyboard: _English (US)_
- Language Support: _English (United States)_
- Time & Date: _Europe/Warsaw timezone_
- Installation Source: _Closest mirror_
- Software Selection: _Server_
- Installation Destination: _Automatic partitioning selected_
- KDUMP: _Kdump is enabled_
- Network & Host Name: _Connected_ `enp0s3`
- Security Profile: _No profile selected_
- Root Account: _Root password is set_
- User Creation: _Administrator_ `$myuser` _will be created_

# Termux

`curl -sL ksz.gitlab.io/te | sh`

# Windows

**Windows 7:**  
`(New-Object Net.WebClient).DownloadString("https://ksz.gitlab.io/wi") | Invoke-Expression`

**Windows 10/11:**  
`iwr [-useb] ksz.gitlab.io/wi | iex`
